#!/usr/bin/env php

<?php
/**
 * Tine 2.0 migration script
 * - This script separate domain data from global data configuration
 * - This is a necessary procedure for adopting of multidomain.
 *
 * @package     HelperScripts
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Flávio Gomes da Silva Lisboa <flavio.lisboa@serpro.gov.br>
 * @copyright   Copyright (c) 2015 Serpro (http://www.serpro.gov.br)
 * @version     $Id$
 *
 */

// for migrating from a folder different of tine20, append t=[folder name] to commmand line, for example, t=expressov3
define('APPNAME', getAppName($argv));

main();

/**
 * Start of migration process
 */
function main()
{

    prepareEnvironment();

    try {
        $opts = new Zend_Console_Getopt(array(
                'domain|d-s' => 'separate data from config file of specified domain',
                'help|h'     => 'help option with no required parameter'
        ));
        $opts->parse();
    } catch (Zend_Console_Getopt_Exception $e) {
        echo $e->getUsageMessage() . "\nType t=[target] for a folder different of tine20\n";
        exit;
    }

    if($opts->getOption('h')) {
        die("ERROR: ".$opts->getUsageMessage() . "\nType t=[target] for a folder different of tine20\n");
    }

    $domain = $opts->getOption('d');
    if (empty($domain)) die("\nDomain was not specified!\n");

    echo "\nDATA FROM CONFIGURATION WILL BE MOVED AND WILL BE OVERWRITTEN IF THEY ARE DUPLICATED!\n";
    echo "\nDID YOU BACKUP YOUR DATA?\n";
    echo "\nDO YOU WANT TO CONTINUE? (yes/no)\n";
    $handle = fopen('php://stdin','r');
    $line = fgets($handle);
    $line = trim(strtolower($line));
    if ($line !== 'yes')
    {
        if ($line !== 'no'){
            echo "\nOk, you didn't understand... try again, but read carefully instructions, please\n";
        }
        die("\nAborting separation...\n");
    }

    separateConfigData($domain);
}

/**
 *
 * @param string $domain
 */
function separateConfigData($domain)
{
    $globalData = array();
    $domainData = array();

    $pathToGlobalFile = realpath(__DIR__ . '/../' . APPNAME);
    $pathToDomainFile = realpath(__DIR__ . '/../' . APPNAME . '/domains/' . $domain);

    $globalFile = $pathToGlobalFile . '/config.inc.php';
    $domainFile = $pathToDomainFile . '/config.inc.php';

    $currentGlobalData = require $globalFile;
    $currentDomainData = require $domainFile;

    $cfg = Tinebase_Config::getInstance();

    // read current global data
    foreach ($currentGlobalData as $configKey => $configValue){
        if ($cfg->isDomainData($configKey)){
            $domainData[$configKey] = $configValue;
        } else {
            $globalData[$configKey] = $configValue;
        }
    }

    // read current domain data
    foreach ($currentDomainData as $configKey => $configValue){
        if ($cfg->isDomainData($configKey)){
            $domainData[$configKey] = $configValue;
        } else {
            $globalData[$configKey] = $configValue;
        }
    }

    $config = new Zend_Config($globalData);
    $writer = new Zend_Config_Writer_Array(array(
        'config'   => $config,
        'filename' => $globalFile
    ));

    // save former global configuration in a temporary file
    $formerGlobalFile = $globalFile . '.old';
    echo "\Storing former global configuration into $formerGlobalFile...\n";
    copy($globalFile, $formerGlobalFile);
    try {
        echo "\Recording global data...\n";
        $writer->write();
    } catch (Exception $e) {
        echo "\Restoring former global configuration from $formerGlobalFile...\n";
        unlink($globalFile);
        copy($formerGlobalFile,$globalFile);
        unlink($formerGlobalFile);
        die("\nDid't finish separation because " . $e->getMessage() . "\n");
    }

    $config = new Zend_Config($domainData);
    $writer = new Zend_Config_Writer_Array(array(
        'config'   => $config,
        'filename' => $domainFile
    ));

    // save former domain configuration in a temporary file
    $formerDomainFile = $domainFile . '.old';
    echo "\Storing former domain configuration into $formerDomainFile...\n";
    copy($domainFile, $formerDomainFile);
    try {
        echo "\Recording data of domain $domain...\n";
        $writer->write();
    } catch (Exception $e) {
        echo "\Restoring former global and domain configuration...\n";
        unlink($globalFile);
        copy($formerGlobalFile,$globalFile);
        unlink($domainFile);
        copy($formerDomainFile,$domainFile);
        unlink($formerGlobalFile);
        unlink($formerDomainFile);
        die("\nDid't finish separation because " . $e->getMessage() . "\n");
    }
    echo "\Deleting $formerGlobalFile...\n";
    unlink($formerGlobalFile);
    echo "\Deleting $formerDomainFile...\n";
    unlink($formerDomainFile);

    echo "\nSeparation finished.\n";
}

/**
 * Sets the include path and loads autoloader classes
 */
function prepareEnvironment()
{
    $paths = array(
            realpath(__DIR__ . '/../' . APPNAME),
            realpath(__DIR__ . '/../' . APPNAME . '/library'),
            get_include_path()
    );
    set_include_path(implode(PATH_SEPARATOR, $paths));

    require_once 'Zend/Loader/Autoloader.php';
    $autoloader = Zend_Loader_Autoloader::getInstance();
    $autoloader->setFallbackAutoloader(true);
    Tinebase_Autoloader::initialize($autoloader);

    // plugins can change scope of configuration data
    require 'init_plugins.php';
}

/**
 * get installation folder name
 * feed constant APPNAME
 *
 * @param array $argv
 * @return string
 */
function getAppName(array $argv)
{
    $appName = 'tine20';
    foreach ($argv as $arg){
        if (substr($arg, 0, 2) == 't='){
            $appName = trim(substr($arg, 2));
            break;
        }
    }

    return $appName;
}