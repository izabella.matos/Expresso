/*
 * TODO: Licensing
 */
package br.gov.serpro.expresso.security.cert;

import java.awt.Frame;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.AuthProvider;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.CommandMap;
import javax.activation.MailcapCommandMap;
import javax.mail.MessagingException;
import javax.mail.internet.HeaderTokenizer;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimePart;
import javax.security.auth.login.LoginException;
import javax.swing.JOptionPane;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.asn1.cms.IssuerAndSerialNumber;
import org.bouncycastle.asn1.smime.SMIMECapabilitiesAttribute;
import org.bouncycastle.asn1.smime.SMIMECapability;
import org.bouncycastle.asn1.smime.SMIMECapabilityVector;
import org.bouncycastle.asn1.smime.SMIMEEncryptionKeyPreferenceAttribute;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x500.style.IETFUtils;
import org.bouncycastle.cert.jcajce.JcaCertStore;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.bouncycastle.cms.CMSAlgorithm;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.RecipientId;
import org.bouncycastle.cms.RecipientInformation;
import org.bouncycastle.cms.RecipientInformationStore;
import org.bouncycastle.cms.jcajce.JcaSimpleSignerInfoGeneratorBuilder;
import org.bouncycastle.cms.jcajce.JceCMSContentEncryptorBuilder;
import org.bouncycastle.cms.jcajce.JceKeyTransEnvelopedRecipient;
import org.bouncycastle.cms.jcajce.JceKeyTransRecipientId;
import org.bouncycastle.cms.jcajce.JceKeyTransRecipientInfoGenerator;
import org.bouncycastle.mail.smime.SMIMEEnveloped;
import org.bouncycastle.mail.smime.SMIMEEnvelopedGenerator;
import org.bouncycastle.mail.smime.SMIMEException;
import org.bouncycastle.mail.smime.SMIMESignedGenerator;
import org.bouncycastle.mail.smime.SMIMEUtil;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.util.Store;
import br.gov.serpro.expresso.security.exception.ExpressoCertificateException;
import br.gov.serpro.expresso.security.setup.Setup;
import br.gov.serpro.expresso.security.ui.DialogBuilder;
import br.gov.serpro.expresso.security.util.Base64Utils;

/**
 * Classe que realiza todo o trabalho realizado com o certificado
 *
 * @author Mário César Kolling - mario.kolling@serpro.gov.br
 */
//TODO: Criar exceções para serem lançadas, entre elas DigitalCertificateNotLoaded
public class DigitalCertificate {

    private static final Logger logger = Logger.getLogger(DigitalCertificate.class.getName());

    private final Frame parentFrame;
    private final Setup setup;
    private final String currentAccountFullName;
    private final String currentAccountEmailAddress;

    private TokenCollection tokens;
    private String selectedCertificateAlias;
    private KeyStore keyStore; // KeyStore que guarda o certificado do usuário. Pode ser nulo.
    private String providerName; // Nome do SecurityProvider pkcs11 carregado. Pode ser nulo.
    private boolean keystoreLoaded = false;

    private static final String HOME_SUBDIR; // Subdiretório dentro do diretório home do usuário. Dependente de SO.
    private static final String EPASS_2000; // Caminho da biblioteca do token ePass2000. Dependente de SO.
    private static final String CRLF = "\r\n"; // Separa campos na resposta do serviço de verificação de certificados
    private static final String SUBJECT_ALTERNATIVE_NAME = "2.5.29.17"; // Não é mais utilizado.
    private static final URL[] TRUST_STORES_URLS = new URL[3]; // URLs (file:/) das TrustStores, cacerts (jre),
    // trusted.certs e trusted.jssecerts (home do usuário)
    // Utilizadas para validação do certificado do servidor.
    private static final String[] TRUST_STORES_PASSWORDS = null; // Senhas para cada uma das TrustStores,
    // caso seja necessário.
    private static boolean useMSCapi = false;

    /*
     * Bloco estático que define os caminhos padrões da instalação da jre, do
     * diretório home do usuário, e da biblioteca de sistema do token ePass2000,
     * de acordo com o sistema operacional.
     */
    static {

        Properties systemProperties = System.getProperties();
        Map<String, String> env = System.getenv();

        if (systemProperties.getProperty("os.name").equalsIgnoreCase("linux")) {
            HOME_SUBDIR = "/.java/deployment/security";
            EPASS_2000 = "/usr/lib/libepsng_p11.so";
        } else {
            HOME_SUBDIR = "\\dados de aplicativos\\sun\\java\\deployment\\security";
            EPASS_2000 = System.getenv("SystemRoot") + "\\system32\\ngp11v211.dll";
            DigitalCertificate.useMSCapi = false;
        }

        try {
            if (systemProperties.getProperty("os.name").equalsIgnoreCase("linux")) {
                TRUST_STORES_URLS[0] = new File(systemProperties.getProperty("java.home") + "/lib/security/cacerts").toURI().toURL();
                TRUST_STORES_URLS[1] = new File(systemProperties.getProperty("user.home") + HOME_SUBDIR + "/trusted.certs").toURI().toURL();
                TRUST_STORES_URLS[2] = new File(systemProperties.getProperty("user.home") + HOME_SUBDIR + "/trusted.jssecerts").toURI().toURL();
            } else {

                TRUST_STORES_URLS[0] = new File(systemProperties.getProperty("java.home") + "\\lib\\security\\cacerts").toURI().toURL();
                TRUST_STORES_URLS[1] = new File(systemProperties.getProperty("user.home") + HOME_SUBDIR + "\\trusted.certs").toURI().toURL();
                TRUST_STORES_URLS[2] = new File(systemProperties.getProperty("user.home") + HOME_SUBDIR + "\\trusted.jssecerts").toURI().toURL();
            }

            // Define os tipos smime no mailcap
            MailcapCommandMap mailcap = (MailcapCommandMap) CommandMap.getDefaultCommandMap();

            mailcap.addMailcap("application/pkcs7-signature;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.pkcs7_signature");
            mailcap.addMailcap("application/pkcs7-mime;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.pkcs7_mime");
            mailcap.addMailcap("application/x-pkcs7-signature;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.x_pkcs7_signature");
            mailcap.addMailcap("application/x-pkcs7-mime;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.x_pkcs7_mime");
            mailcap.addMailcap("multipart/signed;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.multipart_signed");

            CommandMap.setDefaultCommandMap(mailcap);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     */
    public DigitalCertificate() {
        this(null, null, null, null);
    }

    /**
     * 
     */
    public DigitalCertificate(Setup setup) {
        this(null, setup, null, null);        
    }

    public DigitalCertificate(Frame parent, Setup setup, String currentAccountFullName, String currentAccountEmailAddress) {
        this.parentFrame = parent;
        this.setup = setup;
        this.currentAccountFullName = currentAccountFullName;
        this.currentAccountEmailAddress = currentAccountEmailAddress;
    }

    boolean isKeystoreLoaded() {
        return keystoreLoaded;
    }

    static boolean isUseMSCapi() {
        return useMSCapi;
    }
    
    PrivateKey getPrivateKey() throws GeneralSecurityException {
        
        KeyStore.Entry ke = keyStore.getEntry(selectedCertificateAlias, null);
        if (!(ke instanceof KeyStore.PrivateKeyEntry))
            throw new RuntimeException("The entry is not a private key.");
        return ((KeyStore.PrivateKeyEntry) ke).getPrivateKey();
    }
    
    /**
     * @return the cert
     */
    public Certificate getDigitalCertificate() throws KeyStoreException {
        return keyStore.getCertificate(selectedCertificateAlias);
    }

    /**
     * Get a PEM encoded instance of the user certificate
     *
     * @return PEM encoded Certificate
     * @throws CertificateEncodingException
     */
    public String getPEMCertificate() throws CertificateEncodingException, KeyStoreException {
        if (isKeystoreLoaded()) {
            return Base64Utils.der2pem(getDigitalCertificate().getEncoded(), true);
        }
        return null;

    }

    /**
     * Logout from provider session and remove provider if AuthProvider  
     */
    public void destroy() {

        AuthProvider ap = null;

        if (this.setup.getParameter("debug").equalsIgnoreCase("true")) {
            logger.log(Level.INFO, "logout no provider");
        }

        if (this.keyStore != null && this.keyStore.getProvider() instanceof AuthProvider) {
            ap = (AuthProvider) this.keyStore.getProvider();

            try {
                ap.logout();
            } catch (LoginException e) {
                if (this.setup.getParameter("debug").equalsIgnoreCase("true")) {
                    e.printStackTrace();
                }
            }

            if (this.providerName != null) {
                Security.removeProvider(providerName);
            }
        }

        selectedCertificateAlias = null;
        keyStore = null;
        providerName = null;
        keystoreLoaded = false;
    }

    /**
     * Procura pelo token nos locais padrões (Por enquanto só suporta o token
     * ePass200), senão procura por um certificado A1 em
     * System.getProperties().getProperty("user.home") + HOME_SUBDIR +
     * "/trusted.clientcerts" e retorna um inteiro de acordo com resultado desta
     * procura.
     *
     * @author	Mário César Kolling
     * @see	DigitalCertificate
     */
    public void init() throws ExpressoCertificateException {

        if (!DigitalCertificate.useMSCapi) {
            tokens = new TokenCollection(setup);
        }

        Provider[] providers = Security.getProviders();
        if (this.setup.getParameter("debug").equalsIgnoreCase("true")) {
            for (Provider provider : providers) {
                logger.log(Level.INFO, provider.getInfo());
            }
        }
        
        openKeyStore();

    }

    static public String verifyP7S(MimeMessage body) {
        try {

            MimeMultipart mm = (MimeMultipart) body.getContent();
            externalLoop:
            for (int i = mm.getCount() - 1; i >= 0; i--) {
                MimeBodyPart mbp = (MimeBodyPart) mm.getBodyPart(i);
                if (mbp.getContentType().contains("application/pkcs7-signature")) {
                    String[] contentTypeArray = body.getHeader("Content-Type");
                    HeaderTokenizer hTokenizer = new HeaderTokenizer(contentTypeArray[0]);

                    String boundary = "";
                    HeaderTokenizer.Token t = hTokenizer.next();
                    for (; t.getType() != HeaderTokenizer.Token.EOF; t = hTokenizer.next()) {
                        if (t.getType() == HeaderTokenizer.Token.ATOM && t.getValue().equalsIgnoreCase("boundary=")) {
                            break;
                        }
                    }
                    t = hTokenizer.next();
                    if (t.getType() == HeaderTokenizer.Token.QUOTEDSTRING) {
                        boundary = t.getValue();
                    }

                    InputStreamReader rawStreamReader = new InputStreamReader(mbp.getRawInputStream(), "iso-8859-1");

                    StringBuilder signature = new StringBuilder(256);
                    while (rawStreamReader.ready()) {
                        char[] buffer = new char[256];
                        rawStreamReader.read(buffer);
                        signature.append(buffer);
                    }

                    String[] array = signature.toString().split("\\r\\n|(?<!\\r)\\n");

                    boolean badFormat = false;
                    for (int j = 0; j < array.length; j++) {
                        if (array[j].length() > array[0].length()) {
                            badFormat = true;
                            break;
                        }
                    }

                    signature = null;
                    array = null;

                    if (badFormat) {
                        BufferedInputStream parsedIS = new BufferedInputStream(mbp.getInputStream());
                        ByteArrayOutputStream baos = new ByteArrayOutputStream(parsedIS.available());

                        while (parsedIS.available() > 0) {
                            byte[] buffer = new byte[parsedIS.available()];
                            parsedIS.read(buffer);
                            baos.write(buffer);
                        }

                        Enumeration headers = headers = mbp.getAllHeaderLines();
                        String headersString = "";
                        while (headers.hasMoreElements()) {
                            String header = (String) headers.nextElement();
                            headersString += header + "\r\n";
                        }

                        String base64Encoded = Base64Utils.der2pem(baos.toByteArray(), false);

                        mm.removeBodyPart(i);
                        body.saveChanges();

                        ByteArrayOutputStream oStream = new ByteArrayOutputStream();

                        oStream = new ByteArrayOutputStream();
                        body.writeTo(oStream);

                        BufferedReader reader = new BufferedReader(new StringReader(oStream.toString()));
                        OutputStream os = new ByteArrayOutputStream();

                        String line = "";
                        while ((line = reader.readLine()) != null) {
                            if (!line.equals("--" + boundary + "--")) {
                                os.write((line + "\r\n").getBytes("iso-8859-1"));
                            }
                        }

                        return os.toString()
                                + "--" + boundary + "\r\n" + headersString
                                + "\r\n" + base64Encoded
                                + "--" + boundary + "--\r\n";

                    }

                    break externalLoop;
                }
            }

        } catch (IOException ex) {
            Logger.getLogger(DigitalCertificate.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MessagingException ex) {
            Logger.getLogger(DigitalCertificate.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassCastException ex) {
            Logger.getLogger(DigitalCertificate.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    /**
     * Usado para assinar digitalmente um e-mail.
     *
     * @param body
     * @return String vazia
     */
    public MimeBodyPart signMail(MimeBodyPart body) throws ExpressoCertificateException {

        try {
            PrivateKey privateKey;
            if (isKeystoreLoaded()) {
                privateKey = getPrivateKey();
            } else {
                return null; // canceled
            }

            Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

            X509Certificate certificate = (X509Certificate) getDigitalCertificate();

            /**
            * @todo Add other certificates (certificate chain) to the signer
            */
            List certList = new ArrayList();
            certList.add(certificate);

            //
            // create a CertStore containing the certificates we want carried
            // in the signature
            //
            if (this.setup.getParameter("debug").equalsIgnoreCase("true")) {
                logger.log(Level.INFO, "Provider: {0}", this.providerName);
            }

            Store certs = new JcaCertStore(certList);

            //
            // create some smime capabilities in case someone wants to respond
            //
            ASN1EncodableVector signedAttrs = new ASN1EncodableVector();

            SMIMECapabilityVector caps = new SMIMECapabilityVector();

            caps.addCapability(SMIMECapability.dES_EDE3_CBC);
            caps.addCapability(SMIMECapability.rC2_CBC, 128);
            caps.addCapability(SMIMECapability.dES_CBC);

            SMIMESignedGenerator gen = new SMIMESignedGenerator(body.getEncoding());

            // TODO:  Verificar se este código é suficiente para cumprir a norma.
            if (this.setup.getParameter("debug").equalsIgnoreCase("true")) {
                Provider.Service sha512 = this.keyStore.getProvider().getService("MessageDigest", "SHA-512");
                Provider.Service sha256 = this.keyStore.getProvider().getService("MessageDigest", "SHA-256");

                if (sha512 != null) {
                    logger.log(Level.INFO, "sha512: {0}: {1}", new String[]{sha512.getType(), sha512.getAlgorithm()});
                } else {
                    logger.log(Level.INFO, "sha512: não suportado!");
                }

                if (sha256 != null) {
                    logger.log(Level.INFO, "sha256: {0}: {1}", new String[]{sha256.getType(), sha256.getAlgorithm()});
                } else {
                    logger.log(Level.INFO, "sha256: não suportado!");
                }
            }

            signedAttrs.add(new SMIMECapabilitiesAttribute(caps));

            // add an encryption key preference for encrypted responses - normally this would be different from the signing certificate...
            IssuerAndSerialNumber issuerAndSerial = new IssuerAndSerialNumber(new X500Name(((X509Certificate) certificate).getSubjectDN().getName()),
                    ((X509Certificate) certificate).getSerialNumber());
            signedAttrs.add(new SMIMEEncryptionKeyPreferenceAttribute(issuerAndSerial));

            // add a signer to the generator - this specifies we are using SHA1 and
            // adding the smime attributes above to the signed attributes that
            // will be generated as part of the signature. The encryption algorithm
            // used is taken from the key - in this RSA with PKCS1Padding

            // TODO: Verificar problema com MessageDigest SHA-512.
    //        if (this.keyStore.getProvider().getService("MessageDigest", "SHA-512") != null){
    //            gen.addSignerInfoGenerator(new JcaSimpleSignerInfoGeneratorBuilder().setSignedAttributeGenerator(new AttributeTable(signedAttrs)).build(SMIMESignedGenerator.DIGEST_SHA512, (PrivateKey) privateKey,
    //                (X509Certificate) certificate));
    //        }
    //        else
            if (this.keyStore.getProvider().getService("MessageDigest", "SHA-256") != null) {
                gen.addSignerInfoGenerator(new JcaSimpleSignerInfoGeneratorBuilder().setSignedAttributeGenerator(
                        new AttributeTable(signedAttrs)).build(
                        "SHA256withRSA", privateKey, (X509Certificate) certificate));
            } else { // Falls back to SHA1
                gen.addSignerInfoGenerator(new JcaSimpleSignerInfoGeneratorBuilder().setSignedAttributeGenerator(
                        new AttributeTable(signedAttrs)).build(
                        "SHA1withRSA", privateKey, (X509Certificate) certificate));
            }

            gen.addCertificates(certs);

            //TODO: Extrair todos os headers de unsignedMessage
            // Gera a assinatura        
            MimeMultipart mimeMultiPart = gen.generate(body);
            MimeBodyPart mimeBodyPart = new MimeBodyPart();
            mimeBodyPart.setContent(mimeMultiPart);
            return mimeBodyPart;
            
        } catch (SMIMEException ex) {
            throw new ExpressoCertificateException(ex.getMessage(), ex);
        } catch (MessagingException ex) {
            throw new ExpressoCertificateException(ex.getMessage(), ex);
        } catch (GeneralSecurityException ex) {
            throw new ExpressoCertificateException(ex.getMessage(), ex);
        }   catch (OperatorCreationException ex) {
            throw new ExpressoCertificateException(ex.getMessage(), ex);
        }
    }

    /**
     *
     * @param certificates
     * @param msg
     * @return
     * @throws ExpressoCertificateException
     */
    public MimeBodyPart createEncryptedBodyPart(List<X509Certificate> certificates, MimeBodyPart msg) throws ExpressoCertificateException {
        try {
            /*
             * Create the encrypter
             */
            SMIMEEnvelopedGenerator gen = new SMIMEEnvelopedGenerator();

            for (X509Certificate x509Certificate : certificates) {
                gen.addRecipientInfoGenerator(new JceKeyTransRecipientInfoGenerator(x509Certificate));
            }

            /*
             * Encrypt the message
             */
            MimeBodyPart encryptedPart = gen.generate(msg, new JceCMSContentEncryptorBuilder(CMSAlgorithm.AES256_CBC).build());

            return encryptedPart;
        } catch (SMIMEException e) {
            throw new ExpressoCertificateException(e.getMessage(), e);
        } catch (CMSException e) {
            throw new ExpressoCertificateException(e.getMessage(), e);
        } catch (CertificateEncodingException e) {
            throw new ExpressoCertificateException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param certificate
     * @param privateKey
     * @param is
     * @return
     * @throws ExpressoCertificateException
     */
    public MimeBodyPart readEncryptedMail(MimePart mimeMessage) throws ExpressoCertificateException {

        try {

            PrivateKey privateKey;
            if (isKeystoreLoaded()) {
                privateKey = getPrivateKey();
            } else {
                return null; // canceled
            }

            X509Certificate certificate = (X509Certificate) getDigitalCertificate();

            // Generate a suitable recipient identifier.
            RecipientId recId = new JceKeyTransRecipientId(certificate);
//
//                // Get a Session object with the default properties.
//                Properties props = System.getProperties();
//
//                Session session = Session.getDefaultInstance(props, null);
//
//                MimeMessage mimeMessage = new MimeMessage(session, is);

            //Multipart mpEncrypted = (Multipart) mimeMessage.getContent();
            SMIMEEnveloped m = (mimeMessage instanceof MimeMessage)
                    ? new SMIMEEnveloped((MimeMessage) mimeMessage)
                    : new SMIMEEnveloped((MimeBodyPart) mimeMessage);


            RecipientInformationStore recipients = m.getRecipientInfos();
            RecipientInformation recipient = recipients.get(recId);
            if (recipient == null) {
                throw new ExpressoCertificateException("The Recipient Information submitted does not match the selection.");
            }

            MimeBodyPart res = SMIMEUtil.toMimeBodyPart(recipient.getContent(new JceKeyTransEnvelopedRecipient(privateKey)));
            //mimeMessage.setContent(res, res.getContentType());

//                logger.log(Level.INFO, "Message Contents");
//                logger.log(Level.INFO, "----------------");
//                logger.log(Level.INFO, res.getContent().toString());

            return res;


        } catch (GeneralSecurityException ex) {
            throw new ExpressoCertificateException(ex.getMessage(), ex);
        } catch (MessagingException ex) {
            throw new ExpressoCertificateException(ex.getMessage(), ex);
        } catch (SMIMEException ex) {
            throw new ExpressoCertificateException(ex.getMessage(), ex);
        } catch (CMSException ex) {
            throw new ExpressoCertificateException(ex.getMessage(), ex);
        }
    }

    /**
     * Carrega um novo SecurityProvider
     *
     * @throws KeyStoreException Quando não conseguir iniciar a KeyStore, ou a
     * lib do Token ou Smartcard não foi encontrada, ou o usuário não inseriu o
     * Token.
     */
    private void loadKeyStore() throws KeyStoreException, NoSuchProviderException, CertificateException, NoSuchAlgorithmException, IOException {
        
        if (useMSCapi) {
            keyStore = KeyStore.getInstance("Windows-MY", "SunMSCAPI");
            keyStore.load(null, null);
                    
        } else {
            KeyStore.CallbackHandlerProtection chp = new KeyStore.CallbackHandlerProtection(new PinCallbackHandler(parentFrame, setup));
            KeyStore.Builder keystoreBuilder = KeyStore.Builder.newInstance("PKCS11", null, chp);
            keyStore = keystoreBuilder.getKeyStore();
        }
    }
    
    /**
     * Opens up the keystore
     *
     * @todo break this method into Keystore getkeystore(), String
     * getCertificateEntry.
     */
    private void openKeyStore() throws ExpressoCertificateException {

        outer: while (true) {
            try {
                loadKeyStore();

                //select digital certificate entry
                String alias = DialogBuilder.showCertificateSelector(parentFrame,
                        setup, this.getAliasesList());

                if (alias != null) {

                    if (setup.getParameter("debug").equalsIgnoreCase("true")) {
                        logger.log(Level.INFO, "Selected Alias: {0}", alias);
                        logger.log(Level.INFO, "Aliases ({0}): ", String.valueOf(keyStore.size()));
                        for (Enumeration e = keyStore.aliases(); e.hasMoreElements();) {
                            logger.log(Level.INFO, "\t{0}", (String) e.nextElement());
                        }
                    }

                    X509Certificate x509cert = (X509Certificate) this.keyStore.getCertificate(alias);

                    String certSubjectAlternativeRFC822Name = null;
                    Collection<List<?>> subjectAltNames = x509cert.getSubjectAlternativeNames();
                    if(subjectAltNames != null) {
                        for(List<?> q : subjectAltNames) {
                            int nameType = (Integer) q.get(0);
                            if(nameType == 1 /*rfc822Name*/ ) {
                                certSubjectAlternativeRFC822Name = (String) q.get(1);
                            }
                        }
                    }

                    X500Name x500name = new JcaX509CertificateHolder(x509cert).getSubject();
                    String certSubjectCommonName = IETFUtils.valueToString(x500name.getRDNs(BCStyle.CN)[0].getFirst().getValue());

                    logger.log(Level.INFO, "certSubjectCommonName: {0}", certSubjectCommonName);
                    logger.log(Level.INFO, "certSubjectAlternativeRFC822Name: {0}", certSubjectAlternativeRFC822Name);
                    logger.log(Level.INFO, "currentAccountFullName: {0}", currentAccountFullName);
                    logger.log(Level.INFO, "currentAccountEmailAddress: {0}", currentAccountEmailAddress);

                    boolean validCert = true;

                    if(currentAccountEmailAddress != null) {
                        if((certSubjectAlternativeRFC822Name == null)
                                || (! currentAccountEmailAddress.trim().equalsIgnoreCase(certSubjectAlternativeRFC822Name.trim()))) {

                            validCert = false;
                        }
                    }

//                    if(currentAccountFullName != null) {
//                        if( ! currentAccountFullName.trim().equalsIgnoreCase(certSubjectCommonName.trim())) {
//                            
//                            validCert = false;
//                        }
//                    }

                    if( ! validCert ) {
                        String errorMsgFormat = setup.getLang("ExpressoCertMessages", "DigitalCertificate005");
                        
                        String errorMsg = String.format(errorMsgFormat,
                                certSubjectAlternativeRFC822Name,
                                certSubjectCommonName,
                                currentAccountEmailAddress,
                                currentAccountFullName);

                        DialogBuilder.showMessageDialog(
                                parentFrame,
                                errorMsg,
                                JOptionPane.ERROR_MESSAGE,
                                setup);

                        //keystoreLoaded = false;
                        continue;
                    }
                    else {
                        if(false) {
                        //if(setup.getParameter("debug").equalsIgnoreCase("true")) {

                            String successMsgFormat = setup.getLang("ExpressoCertMessages", "DigitalCertificate006");

                            String successMsg = String.format(successMsgFormat,
                                    certSubjectAlternativeRFC822Name,
                                    certSubjectCommonName,
                                    currentAccountEmailAddress,
                                    currentAccountFullName);

                            DialogBuilder.showMessageDialog(
                                    parentFrame,
                                    successMsg,
                                    JOptionPane.INFORMATION_MESSAGE,
                                    setup);
                        }
                    }

                    selectedCertificateAlias = alias;

//                    // getting private key
//                    Key privateKey = keyStore.getKey(selectedCertificateAlias, null);
//                    if (privateKey == null) {
//                        DialogBuilder.showMessageDialog(parentFrame, setup.getLang("ExpressoCertMessages", "DigitalCertificate004"), setup);
//                    }
                    keystoreLoaded = true;
                } else {
                    keystoreLoaded = false; // cancelled
                }
                break;
            } catch (IOException ex) {
                throw new ExpressoCertificateException("Error getting certificate entries", ex);
            } catch (GeneralSecurityException ex){
                List<String> exceptions = Arrays.asList(new String[] {
                    "br.gov.serpro.expresso.security.exception.DialogCancelledException", // 0
                    "java.security.NoSuchAlgorithmException", // 1
                    "sun.security.pkcs11.wrapper.PKCS11Exception", // 2
                });
                for (Throwable cause = ex.getCause(), previous = null; 
                        cause != previous && cause != null;
                        previous = cause, cause = cause.getCause()) {
                    
                    String name = cause.getClass().getName();
                    switch (exceptions.indexOf(name)) {
                        case 0 : // cancelled
                            keystoreLoaded = false;
                            break outer;
                        case 1 : // kestore not found
                            DialogBuilder.showMessageDialog(
                                parentFrame,
                                setup.getLang("ExpressoCertMessages", "DigitalCertificate001"),
                                setup);
                            keystoreLoaded = false;
                            break outer;
                        case 2 : // wrong pin
                            DialogBuilder.showMessageDialog(
                                parentFrame,
                                setup.getLang("ExpressoCertMessages", "DigitalCertificate002"),
                                setup);
                            continue outer;
                    }
                }
                logger.log(Level.INFO, "Error openning keystore {0}", ex);
                throw new ExpressoCertificateException("Error openning keystore", ex);
            }
        }
    }

    Map<String, String> getAliasesList() throws IOException, KeyStoreException {

        if (setup.getParameter("debug").equalsIgnoreCase("true")) {
            logger.log(Level.INFO, "Getting Aliases");
        }

        Map<String, String> aliases = new HashMap<String, String>();

        for (Enumeration<String> al = this.keyStore.aliases(); al.hasMoreElements();) {
            String alias = al.nextElement();
            X509Certificate certObj = (X509Certificate) this.keyStore.getCertificate(alias);

            StringBuilder selector = new StringBuilder();
            // get more info to generate the value
            // Subject's CN / Issuer's CN / Expiration Data
            String subject = certObj.getSubjectX500Principal().getName();
            int pInicial = subject.indexOf('=') + 1;
            int pFinal = subject.indexOf(',', pInicial);
            selector.append(subject.substring(pInicial, pFinal)).append(" | ");

            String issuer = certObj.getIssuerX500Principal().getName();
            pInicial = issuer.indexOf('=') + 1;
            pFinal = issuer.indexOf(',', pInicial);
            selector.append(issuer.substring(pInicial, pFinal)).append(" | ");

            // TODO: get the system locale
            Locale locale = new Locale("pt", "BR");
            DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM, locale);
            selector.append(df.format(certObj.getNotAfter())).append(" | ");

            selector.append("(").append(certObj.getSerialNumber()).append(")");

            aliases.put(alias, selector.toString());

        }

        return aliases;
    }
}
