package br.gov.serpro.expresso.security.applet.openmessage;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.regex.Pattern;
import org.apache.james.mime4j.dom.BinaryBody;
import org.apache.james.mime4j.dom.Body;
import org.apache.james.mime4j.dom.Entity;
import org.apache.james.mime4j.dom.Header;
import org.apache.james.mime4j.dom.Message;
import org.apache.james.mime4j.dom.Multipart;
import org.apache.james.mime4j.dom.SingleBody;
import org.apache.james.mime4j.dom.TextBody;
import org.apache.james.mime4j.dom.field.AddressListField;
import org.apache.james.mime4j.dom.field.ContentDescriptionField;
import org.apache.james.mime4j.dom.field.ContentDispositionField;
import org.apache.james.mime4j.dom.field.ContentIdField;
import org.apache.james.mime4j.dom.field.ContentLanguageField;
import org.apache.james.mime4j.dom.field.ContentLengthField;
import org.apache.james.mime4j.dom.field.ContentLocationField;
import org.apache.james.mime4j.dom.field.ContentMD5Field;
import org.apache.james.mime4j.dom.field.ContentTransferEncodingField;
import org.apache.james.mime4j.dom.field.ContentTypeField;
import org.apache.james.mime4j.dom.field.DateTimeField;
import org.apache.james.mime4j.dom.field.MailboxField;
import org.apache.james.mime4j.dom.field.MailboxListField;
import org.apache.james.mime4j.dom.field.MimeVersionField;
import org.apache.james.mime4j.dom.field.UnstructuredField;
import org.apache.james.mime4j.message.BodyPart;
import org.apache.james.mime4j.stream.Field;

public final class OpenUtils {

    private OpenUtils() {}

    public static void traverseWithDefaultAlternativePriority(Entity entity, BodyHandler handler) {
        traverse(entity, handler, "multipart/*", "message/*", "text/html", "text/plain", "text/*");
    }

    public static void traverse(Entity entity, BodyHandler handler, String... alternativeMimeTypesPriority) {
        List<Pattern> patterns = new ArrayList<Pattern>();
        for(String s : alternativeMimeTypesPriority) {
            patterns.add(Pattern.compile("^" + s.trim().replaceAll("\\*", "(.+)") + "$", Pattern.CASE_INSENSITIVE));
        }
        ArrayDeque<Integer> level = new ArrayDeque<Integer>();
        level.add(1);
        traverse(entity, handler, level, patterns.toArray(new Pattern[patterns.size()]));
    }

    private static void traverse(Entity entity, BodyHandler handler, ArrayDeque<Integer> level, Pattern... alternativeMimeTypesPriority) {

        if(handler.isQuitted()) {
            return;
        }

        final EntityHandler entityHandler = (EntityHandler) ((handler instanceof EntityHandler) ? handler : null);

        Header header = entity.getHeader();

        if(entityHandler != null) {

            entityHandler.entity(entity, level);

            if(entity instanceof Message) {
                Message message = (Message) entity;
                if(message.getParent() == null) {
                    entityHandler.message(message, level);
                }
            }
            else if(entity instanceof BodyPart) {
                BodyPart bodyPart = (BodyPart) entity;
                entityHandler.bodyPart(bodyPart, level);
            }

            //------------------------------------------------------------------
            entityHandler.header(header, level);
            for(Field field : header.getFields()) {
                entityHandler.field(field, level);

                if(field instanceof AddressListField) {
                    AddressListField addressListField = (AddressListField) field;
                    entityHandler.addressList(addressListField, level);
                    if("to".equalsIgnoreCase(field.getName())) {
                        entityHandler.to(addressListField, level);
                    }
                    else if("cc".equalsIgnoreCase(field.getName())) {
                        entityHandler.cc(addressListField, level);
                    }
                    else if("bcc".equalsIgnoreCase(field.getName())) {
                        entityHandler.bcc(addressListField, level);
                    }
                    else if("resent-to".equalsIgnoreCase(field.getName())) {
                        entityHandler.resentTo(addressListField, level);
                    }
                    else if("resent-cc".equalsIgnoreCase(field.getName())) {
                        entityHandler.resentCc(addressListField, level);
                    }
                    else if("resent-bcc".equalsIgnoreCase(field.getName())) {
                        entityHandler.resentBcc(addressListField, level);
                    }
                    else if("reply-to".equalsIgnoreCase(field.getName())) {
                        entityHandler.replyTo(addressListField, level);
                    }
                }
                else if(field instanceof ContentDescriptionField) {
                    entityHandler.contentDescription((ContentDescriptionField) field, level);
                }
                else if(field instanceof ContentDispositionField) {
                    entityHandler.contentDisposition((ContentDispositionField) field, level);
                }
                else if(field instanceof ContentIdField) {
                    entityHandler.contentId((ContentIdField) field, level);
                }
                else if(field instanceof ContentLanguageField) {
                    entityHandler.contentLanguage((ContentLanguageField) field, level);
                }
                else if(field instanceof ContentLengthField) {
                    entityHandler.contentLength((ContentLengthField) field, level);
                }
                else if(field instanceof ContentLocationField) {
                    entityHandler.contentLocation((ContentLocationField) field, level);
                }
                else if(field instanceof ContentMD5Field) {
                    entityHandler.contentMD5((ContentMD5Field) field, level);
                }
                else if(field instanceof ContentTransferEncodingField) {
                    entityHandler.contentTransferEncoding((ContentTransferEncodingField) field, level);
                }
                else if(field instanceof ContentTypeField) {
                    entityHandler.contentType((ContentTypeField) field, level);
                }
                else if(field instanceof DateTimeField) {
                    DateTimeField dateTimeField = (DateTimeField) field;
                    entityHandler.dateTime(dateTimeField, level);
                    if("date".equalsIgnoreCase(field.getName())) {
                        entityHandler.date(dateTimeField, level);
                    }
                    else if("resent-date".equalsIgnoreCase(field.getName())) {
                        entityHandler.resentDate(dateTimeField, level);
                    }
                }
                else if(field instanceof MailboxField) {
                    MailboxField mailboxField = (MailboxField) field;
                    entityHandler.mailbox(mailboxField, level);
                    if("sender".equalsIgnoreCase(field.getName())) {
                        entityHandler.sender(mailboxField, level);
                    }
                    else if("resent-sender".equalsIgnoreCase(field.getName())) {
                        entityHandler.resentSender(mailboxField, level);
                    }
                }
                else if(field instanceof MailboxListField) {
                    MailboxListField mailboxListField = (MailboxListField) field;
                    entityHandler.mailboxList(mailboxListField, level);
                    if("from".equalsIgnoreCase(field.getName())) {
                        entityHandler.from(mailboxListField, level);
                    }
                    else if("resent-from".equalsIgnoreCase(field.getName())) {
                        entityHandler.resentFrom(mailboxListField, level);
                    }
                }
                else if(field instanceof MimeVersionField) {
                    entityHandler.mimeVersion((MimeVersionField) field, level);
                }
                else if(field instanceof UnstructuredField) {
                    UnstructuredField unstructuredField = (UnstructuredField) field;
                    entityHandler.unstructured(unstructuredField, level);
                    if("subject".equalsIgnoreCase(field.getName())) {
                        entityHandler.subject(unstructuredField, level);
                    }
                    else if("message-id".equalsIgnoreCase(field.getName())) {
                        entityHandler.messageId(unstructuredField, level);
                    }
                }
            }
            entityHandler.endHeader(header, level);
        }

        //----------------------------------------------------------------------
        Body body = entity.getBody();
        handler.body(body, level);

        if(body instanceof SingleBody) {
            SingleBody singleBody = (SingleBody)body;
            handler.singleBody(singleBody, level);

            boolean isTextBody = (singleBody instanceof TextBody);
            boolean isBinaryBody = (singleBody instanceof BinaryBody);

            ContentTypeField contentType = (ContentTypeField) header.getField("content-type");

            boolean isTextType  = (contentType != null) ?
                    "text".equalsIgnoreCase(contentType.getMediaType()) : true;
            boolean isImageType = (contentType != null) ?
                    "image".equalsIgnoreCase(contentType.getMediaType()) : false;
            boolean isAudioType = (contentType != null) ?
                    "audio".equalsIgnoreCase(contentType.getMediaType()) : false;
            boolean isVideoType = (contentType != null) ?
                    "video".equalsIgnoreCase(contentType.getMediaType()) : false;

            ContentDispositionField contentDisposition = (ContentDispositionField) header.getField("content-disposition");

            boolean isInlineDisposition = (contentDisposition != null) ?
                    "inline".equalsIgnoreCase(contentDisposition.getDispositionType()) : false;
            boolean isAttachmentDisposition = (contentDisposition != null) ?
                    "attachment".equalsIgnoreCase(contentDisposition.getDispositionType()) : false;


if(isTextBody || isTextType) {
    if(isTextBody) {
    System.out.println("IS_TEXT_BODY");
    }
    if(isTextType) {
    System.out.println("IS_TEXT_TYPE");
    }
    if(isAttachmentDisposition) {
    System.out.println("IS_ATTACHMENT_DISPOSITION");
    }
}
            
            
            
            if(isTextBody && isTextType && !isAttachmentDisposition) {
                handler.textBody((TextBody)singleBody, level);
            }
            else if(isBinaryBody) {
                handler.binaryBody((BinaryBody)singleBody, level);
            }

            if(isInlineDisposition && !isTextType) {
                handler.inlineBody(singleBody, level);
            }
            else if(isAttachmentDisposition) {
                handler.attachmentBody(singleBody, level);
            }
            else if(isImageType || isAudioType || isVideoType) {
                handler.attachmentBody(singleBody, level);
            }
        }
        else if(body instanceof Message) {
            //embedded message (mimetype message/rfc822)
            Message message = (Message)body;
            handler.embedded(message, level);
            level.addLast(1);
            traverse(entity, handler, level.clone(), alternativeMimeTypesPriority);
            if(handler.isQuitted()) {
                return;
            }
            level.removeLast();
            handler.endEmbedded(message, level);
        }
        else if(body instanceof Multipart) {
            Multipart multipart = (Multipart)body;
            handler.multipart(multipart, level);

            ContentTypeField contentType = (ContentTypeField) header.getField("content-type");

            if((alternativeMimeTypesPriority.length > 0) && (contentType.isMimeType("multipart/alternative"))) {

                boolean found = false;
                priority:
                for(Pattern mimeType : alternativeMimeTypesPriority) {
                    int sequence = 0;
                    for(Entity bodyPart : multipart.getBodyParts()) {
                        ++sequence;
                        ContentTypeField bodyPartContentType = (ContentTypeField) bodyPart.getHeader().getField("content-type");
                        if((bodyPartContentType != null) && (mimeType.matcher(bodyPartContentType.getMimeType()).matches())) {
                            found = true;
                            level.addLast(sequence);
                            traverse(bodyPart, handler, level.clone(), alternativeMimeTypesPriority);
                            if(handler.isQuitted()) {
                                return;
                            }
                            level.removeLast();
                            break priority;
                        }
                    }
                }
                if(!found) {
                    //choose the plainest (first) alternative. see RFC2046
                    Entity bodyPart = multipart.getBodyParts().get(0);
                    level.addLast(1);
                    traverse(bodyPart, handler, level.clone(), alternativeMimeTypesPriority);
                    if(handler.isQuitted()) {
                        return;
                    }
                    level.removeLast();
                }
            }
            else {
                int sequence = 0;
                for(Entity bodyPart : multipart.getBodyParts()) {
                    level.addLast(++sequence);
                    traverse(bodyPart, handler, level.clone(), alternativeMimeTypesPriority);
                    if(handler.isQuitted()) {
                        break;
                    }
                    level.removeLast();
                }
            }
            handler.endMultipart(multipart, level);
        }

        //----------------------------------------------------------------------
        if(entityHandler != null) {
            if(entity instanceof Message) {
                Message message = (Message) entity;
                if(message.getParent() == null) {
                    entityHandler.endMessage(message, level);
                }
            }
            else if(entity instanceof BodyPart) {
                BodyPart bodyPart = (BodyPart) entity;
                entityHandler.endBodyPart(bodyPart, level);
            }

            entityHandler.endEntity(entity, level);
        }
    }

    //==========================================================================
    public static String dump(Message message) {

        final StringBuilder sb = new StringBuilder();

        EntityHandler handler;
        handler = new EntityHandler() {

            private void append(Deque<Integer> level) {
                sb.append(" [");
                int k = 0;
                for(Integer n : level) {
                    if(k++ > 0) {
                        sb.append(":");
                    }
                    sb.append(n);
                }
                sb.append("]");
            }

            int i = 0;

            private void indent() {
                for(int k = 0; k < i; k++) {
                    sb.append("|   ");
                }
            }

            @Override
            public void message(Message message, Deque<Integer> level) {
                sb.append("entity(Message)");
                append(level);
                sb.append("\n");
                ++i;
            }

            @Override
            public void endMessage(Message message, Deque<Integer> level) {
                --i;
                sb.append("endEntity(Message)").append("\n");
            }

            @Override
            public void bodyPart(BodyPart bodyPart, Deque<Integer> level) {
                indent();
                sb.append("entity(BodyPart)");
                append(level);
                sb.append("\n");
                ++i;
            }

            @Override
            public void endBodyPart(BodyPart bodyPart, Deque<Integer> level) {
                --i;
                indent();
                sb.append("endEntity(BodyPart)").append("\n");
            }

            @Override
            public void singleBody(SingleBody singleBody, Deque<Integer> level) {
                indent();
                sb.append("body(SingleBody)").append("\n");
            }

            @Override
            public void textBody(TextBody textBody, Deque<Integer> level) {
                indent();
                sb.append("{ body(TextBody) }").append("\n");
            }

            @Override
            public void binaryBody(BinaryBody binaryBody, Deque<Integer> level) {
                indent();
                sb.append("{ body(BinaryBody) }").append("\n");
            }

            @Override
            public void inlineBody(SingleBody singleBody, Deque<Integer> level) {
                indent();
                sb.append("{{ inlineBody(SingleBody) }}").append("\n");
            }

            @Override
            public void attachmentBody(SingleBody singleBody, Deque<Integer> level) {
                indent();
                sb.append("{{ attachmentBody(SingleBody) }}").append("\n");
            }

            @Override
            public void embedded(Message message, Deque<Integer> level) {
                indent();
                sb.append("body(Message)");
                append(level);
                sb.append("\n");
                ++i;
            }

            @Override
            public void endEmbedded(Message message, Deque<Integer> level) {
                --i;
                indent();
                sb.append("endBody(Message)").append("\n");
            }

            @Override
            public void multipart(Multipart multipart, Deque<Integer> level) {
                indent();
                sb.append("body(Multipart)").append("\n");
                ++i;
            }

            @Override
            public void endMultipart(Multipart multipart, Deque<Integer> level) {
                --i;
                indent();
                sb.append("endBody(Multipart)").append("\n");
            }

            @Override
            public void header(Header header, Deque<Integer> level) {
                indent();
                sb.append("header(Header)").append("\n");
                ++i;
            }

            @Override
            public void endHeader(Header header, Deque<Integer> level) {
                --i;
                indent();
                sb.append("endHeader(Header)").append("\n");
            }

            @Override
            public void field(Field field, Deque<Integer> level) {
                indent();
                sb.append(field.getName()).append(": ").append(field.getBody()).append("\n");
            }
        };

        traverse(message, handler);

        return sb.toString();
    }


    //==========================================================================
    public static MailboxField getSender(Header header) {
        return (MailboxField) header.getField("sender");
    }
    public static MailboxField getSender(Entity entity) {
        return getSender(entity.getHeader());
    }
    public static MailboxField getSender(Body body) {
        return getSender(body.getParent());
    }

    public static MailboxField getResentSender(Header header) {
        return (MailboxField) header.getField("resent-sender");
    }
    public static MailboxField getResentSender(Entity entity) {
        return getResentSender(entity.getHeader());
    }
    public static MailboxField getResentSender(Body body) {
        return getResentSender(body.getParent());
    }

    public static MailboxListField getFrom(Header header) {
        return (MailboxListField) header.getField("from");
    }
    public static MailboxListField getFrom(Entity entity) {
        return getFrom(entity.getHeader());
    }
    public static MailboxListField getFrom(Body body) {
        return getFrom(body.getParent());
    }

    public static MailboxListField getResentFrom(Header header) {
        return (MailboxListField) header.getField("resent-from");
    }
    public static MailboxListField getResentFrom(Entity entity) {
        return getResentFrom(entity.getHeader());
    }
    public static MailboxListField getResentFrom(Body body) {
        return getResentFrom(body.getParent());
    }

    public static AddressListField getTo(Header header) {
        return (AddressListField) header.getField("to");
    }
    public static AddressListField getTo(Entity entity) {
        return getTo(entity.getHeader());
    }
    public static AddressListField getTo(Body body) {
        return getTo(body.getParent());
    }

    public static AddressListField getCc(Header header) {
        return (AddressListField) header.getField("cc");
    }
    public static AddressListField getCc(Entity entity) {
        return getCc(entity.getHeader());
    }
    public static AddressListField getCc(Body body) {
        return getCc(body.getParent());
    }

    public static AddressListField getBcc(Header header) {
        return (AddressListField) header.getField("bcc");
    }
    public static AddressListField getBcc(Entity entity) {
        return getBcc(entity.getHeader());
    }
    public static AddressListField getBcc(Body body) {
        return getBcc(body.getParent());
    }

    public static AddressListField getResentTo(Header header) {
        return (AddressListField) header.getField("resent-to");
    }
    public static AddressListField getResentTo(Entity entity) {
        return getResentTo(entity.getHeader());
    }
    public static AddressListField getResentTo(Body body) {
        return getResentTo(body.getParent());
    }

    public static AddressListField getResentCc(Header header) {
        return (AddressListField) header.getField("resent-cc");
    }
    public static AddressListField getResentCc(Entity entity) {
        return getResentCc(entity.getHeader());
    }
    public static AddressListField getResentCc(Body body) {
        return getResentCc(body.getParent());
    }

    public static AddressListField getResentBcc(Header header) {
        return (AddressListField) header.getField("resent-bcc");
    }
    public static AddressListField getResentBcc(Entity entity) {
        return getResentBcc(entity.getHeader());
    }
    public static AddressListField getResentBcc(Body body) {
        return getResentBcc(body.getParent());
    }

    public static AddressListField getReplyTo(Header header) {
        return (AddressListField) header.getField("reply-to");
    }
    public static AddressListField getReplyTo(Entity entity) {
        return getReplyTo(entity.getHeader());
    }
    public static AddressListField getReplyTo(Body body) {
        return getReplyTo(body.getParent());
    }

    public static ContentDescriptionField getContentDescription(Header header) {
        return (ContentDescriptionField) header.getField("content-description");
    }
    public static ContentDescriptionField getContentDescription(Entity entity) {
        return getContentDescription(entity.getHeader());
    }
    public static ContentDescriptionField getContentDescription(Body body) {
        return getContentDescription(body.getParent());
    }

    public static ContentDispositionField getContentDisposition(Header header) {
        return (ContentDispositionField) header.getField("content-disposition");
    }
    public static ContentDispositionField getContentDisposition(Entity entity) {
        return getContentDisposition(entity.getHeader());
    }
    public static ContentDispositionField getContentDisposition(Body body) {
        return getContentDisposition(body.getParent());
    }

    public static ContentIdField getContentId(Header header) {
        return (ContentIdField) header.getField("content-id");
    }
    public static ContentIdField getContentId(Entity entity) {
        return getContentId(entity.getHeader());
    }
    public static ContentIdField getContentId(Body body) {
        return getContentId(body.getParent());
    }

    public static ContentLanguageField getContentLanguage(Header header) {
        return (ContentLanguageField) header.getField("content-language");
    }
    public static ContentLanguageField getContentLanguage(Entity entity) {
        return getContentLanguage(entity.getHeader());
    }
    public static ContentLanguageField getContentLanguage(Body body) {
        return getContentLanguage(body.getParent());
    }

    public static ContentLengthField getContentLength(Header header) {
        return (ContentLengthField) header.getField("content-length");
    }
    public static ContentLengthField getContentLength(Entity entity) {
        return getContentLength(entity.getHeader());
    }
    public static ContentLengthField getContentLength(Body body) {
        return getContentLength(body.getParent());
    }

    public static ContentLocationField getContentLocation(Header header) {
        return (ContentLocationField) header.getField("content-location");
    }
    public static ContentLocationField getContentLocation(Entity entity) {
        return getContentLocation(entity.getHeader());
    }
    public static ContentLocationField getContentLocation(Body body) {
        return getContentLocation(body.getParent());
    }

    public static ContentMD5Field getContentMD5(Header header) {
        return (ContentMD5Field) header.getField("content-md5");
    }
    public static ContentMD5Field getContentMD5(Entity entity) {
        return getContentMD5(entity.getHeader());
    }
    public static ContentMD5Field getContentMD5(Body body) {
        return getContentMD5(body.getParent());
    }

    public static ContentTransferEncodingField getContentTransferEncoding(Header header) {
        return (ContentTransferEncodingField) header.getField("content-transfer-encoding");
    }
    public static ContentTransferEncodingField getContentTransferEncoding(Entity entity) {
        return getContentTransferEncoding(entity.getHeader());
    }
    public static ContentTransferEncodingField getContentTransferEncoding(Body body) {
        return getContentTransferEncoding(body.getParent());
    }

    public static ContentTypeField getContentType(Header header) {
        return (ContentTypeField) header.getField("content-type");
    }
    public static ContentTypeField getContentType(Entity entity) {
        return getContentType(entity.getHeader());
    }
    public static ContentTypeField getContentType(Body body) {
        return getContentType(body.getParent());
    }

    public static DateTimeField getDate(Header header) {
        return (DateTimeField) header.getField("date");
    }
    public static DateTimeField getDate(Entity entity) {
        return getDate(entity.getHeader());
    }
    public static DateTimeField getDate(Body body) {
        return getDate(body.getParent());
    }

    public static DateTimeField getResentDate(Header header) {
        return (DateTimeField) header.getField("resent-date");
    }
    public static DateTimeField getResentDate(Entity entity) {
        return getResentDate(entity.getHeader());
    }
    public static DateTimeField getResentDate(Body body) {
        return getResentDate(body.getParent());
    }

    public static MimeVersionField getMimeVersion(Header header) {
        return (MimeVersionField) header.getField("mime-version");
    }
    public static MimeVersionField getMimeVersion(Entity entity) {
        return getMimeVersion(entity.getHeader());
    }
    public static MimeVersionField getMimeVersion(Body body) {
        return getMimeVersion(body.getParent());
    }

    public static UnstructuredField getSubject(Header header) {
        return (UnstructuredField) header.getField("subject");
    }
    public static UnstructuredField getSubject(Entity entity) {
        return getSubject(entity.getHeader());
    }
    public static UnstructuredField getSubject(Body body) {
        return getSubject(body.getParent());
    }

    public static UnstructuredField getMessageId(Header header) {
        return (UnstructuredField) header.getField("message-id");
    }
    public static UnstructuredField getMessageId(Entity entity) {
        return getMessageId(entity.getHeader());
    }
    public static UnstructuredField getMessageId(Body body) {
        return getMessageId(body.getParent());
    }

    public static ExpressoDataField getExpressoData(Header header) {
        return (ExpressoDataField) header.getField("expresso-data");
    }
    public static ExpressoDataField getExpressoData(Entity entity) {
        return getExpressoData(entity.getHeader());
    }
    public static ExpressoDataField getExpressoData(Body body) {
        return getExpressoData(body.getParent());
    }
}
