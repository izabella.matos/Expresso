package br.gov.serpro.expresso.security.applet.openmessage;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.internet.MimeUtility;
import org.apache.james.mime4j.dom.Body;
import org.apache.james.mime4j.dom.Entity;
import org.apache.james.mime4j.dom.SingleBody;
import org.apache.james.mime4j.dom.field.ContentDispositionField;
import org.apache.james.mime4j.dom.field.ContentIdField;
import org.apache.james.mime4j.dom.field.ContentTypeField;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.util.ByteSequence;
import com.google.common.io.ByteStreams;
import static br.gov.serpro.expresso.security.applet.openmessage.CryptoException.raise;

public class ExpressoDataField implements Field {

    private static final Logger logger = Logger.getLogger(ExpressoDataField.class.getName());

    private final String name = "Expresso-Data";
    private final Entity entity;

    private String eid;
    private String filename;
    private byte[] entitySingleBodyDecoded;

    ExpressoDataField(Entity entity) {
        this.entity = entity;
        entity.getHeader().addField(this);
    }

    @Override
    public ByteSequence getRaw() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getBody() {
        Body body = entity.getBody();
        if(body instanceof SingleBody) {
            return getEid() + "; filename=" + getFilename();
        }
        return null;
    }

    @Override
    public String getName() {
        return name;
    }

    public byte[] getEntitySingleBodyDecoded() {

        if(entitySingleBodyDecoded != null) {
            return entitySingleBodyDecoded;
        }

        SingleBody singleBody = (SingleBody) entity.getBody();
        try {
            entitySingleBodyDecoded = ByteStreams.toByteArray(singleBody.getInputStream());
        }
        catch (IOException ex) {
            raise(ex);
        }

        return entitySingleBodyDecoded;
    }

    public String getEid() {

        if(eid != null) {
            return eid;
        }

        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ex) {}

        md.update(getEntitySingleBodyDecoded());

        char[] hexDigest = new char[32];
        int i = 0;
        for(byte b : md.digest()) {
            hexDigest[i++] = Character.forDigit((char)((b & 0xf0) >>> 4), 16);
            hexDigest[i++] = Character.forDigit((char)(b & 0x0f), 16);
        }

        eid = new String(hexDigest);

        return eid;
    }

    public String getFilename() {

        if(filename != null) {
            return filename;
        }

        ContentDispositionField contentDisposition = (ContentDispositionField) entity.getHeader().getField("content-disposition");
        ContentTypeField contentType = (ContentTypeField) entity.getHeader().getField("content-type");

        if(contentDisposition != null) {
            try {
                filename = contentDisposition.getFilename() != null ?
                    MimeUtility.decodeText(contentDisposition.getFilename()) :
                        contentDisposition.getFilename();
            } catch (UnsupportedEncodingException ex) {
                logger.log(Level.INFO, ex.getMessage(), ex);
            }
        }

        if((filename == null) && (contentType != null)) {
            try {
                filename = contentType.getParameter("name") != null ?
                    MimeUtility.decodeText(contentType.getParameter("name")) :
                        contentType.getParameter("name");
            } catch (UnsupportedEncodingException ex) {
                logger.log(Level.INFO, ex.getMessage(), ex);
            }
        }

        if(filename != null) {
            return filename = checkExtension(filename, contentType);
        }

        ContentIdField contentId = (ContentIdField) entity.getHeader().getField("content-id");
        if(contentId != null) {
            StringBuilder sb = new StringBuilder();
            String text = contentId.getId();
            int beginIdx = -1;
            int endIdx = -1;
            for(int i = 0; i < text.length(); i++) {
                char c = text.charAt(i);
                if(Character.isLetterOrDigit(c)) {
                    sb.append(c);
                    if(beginIdx == -1) {
                        beginIdx = i;
                    }
                    endIdx = i;
                }
                else {
                    sb.append("-");
                }
            }
            filename = sb.substring(beginIdx, endIdx + 1);
        }
        else {
            filename = "part_" + getEid();
        }

        return filename = addExtension(filename, contentType);
    }

    private String checkExtension(String filename, ContentTypeField contentType) {
        if(contentType != null) {
            MediaType mediaType = MediaType.lookup(contentType);
            if(mediaType != null) {
                for(String ext : mediaType.extensions()) {
                    if(filename.endsWith("." + ext)) {
                        return filename;
                    }
                }
                return addExtension(filename, mediaType);
            }
        }
        return filename;
    }

    private String addExtension(String filename, ContentTypeField contentType) {
        if(contentType != null) {
            MediaType mediaType = MediaType.lookup(contentType);
            if(mediaType != null) {
                return addExtension(filename, mediaType);
            }
            else {
                String ext = contentType.getSubType();
                return filename + (filename.endsWith(".") ? ext : "." + ext);
            }
        }
        return filename;
    }

    private String addExtension(String filename, MediaType mediaType) {
        String ext = mediaType.extensions()[0];
        return filename + (filename.endsWith(".") ? ext : "." + ext);
    }
}
