<?php
/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     ActiveSync
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2015 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Emerson Faria Nobre <emerson-faria.nobre@serpro.gov.br>
 */

/**
 * Test class for Syncroton_Command_Ping
 *
 * @package     ActiveSync
 */
class Custom_ActiveSync_Command_PingTests extends ActiveSync_Command_PingTests
{
    /**
     * (non-PHPdoc)
     * @see ActiveSync/ActiveSync_TestCase::setUp()
     */
    protected function setUp()
    {
        TestCase::setUp(); // Grandfather

        // speed up tests
        Syncroton_Registry::set(Syncroton_Registry::PING_TIMEOUT, 1);
        Syncroton_Registry::set(Syncroton_Registry::QUIET_TIME, 1);

        $this->_setGeoData = Addressbook_Controller_Contact::getInstance()->setGeoDataForContacts(FALSE);

        Syncroton_Registry::setDatabase(Tinebase_Core::getDb());
        Syncroton_Registry::setTransactionManager(Tinebase_TransactionManager::getInstance());

        Syncroton_Registry::set(Syncroton_Registry::DEVICEBACKEND,       new Syncroton_Backend_Device(Tinebase_Core::getDb(), SQL_TABLE_PREFIX . 'acsync_'));
        Syncroton_Registry::set(Syncroton_Registry::FOLDERBACKEND,       new Syncroton_Backend_Folder(Tinebase_Core::getDb(), SQL_TABLE_PREFIX . 'acsync_'));
        Syncroton_Registry::set(Syncroton_Registry::SYNCSTATEBACKEND,    new Syncroton_Backend_SyncState(Tinebase_Core::getDb(), SQL_TABLE_PREFIX . 'acsync_'));
        Syncroton_Registry::set(Syncroton_Registry::CONTENTSTATEBACKEND, new Syncroton_Backend_Content(Tinebase_Core::getDb(), SQL_TABLE_PREFIX . 'acsync_'));
        Syncroton_Registry::set('loggerBackend', Tinebase_Core::getLogger());

        Syncroton_Registry::setContactsDataClass('Addressbook_Frontend_ActiveSync');
        Syncroton_Registry::setCalendarDataClass('Calendar_Frontend_ActiveSync');
        Syncroton_Registry::setEmailDataClass(Tinebase_EmailUser_Factory::getMailApplicationName()."_Frontend_ActiveSync"); // TODO: Submit it to Tine20 Community. (task13912)
        Syncroton_Registry::setTasksDataClass('Tasks_Frontend_ActiveSync');

        $this->_device = Syncroton_Registry::getDeviceBackend()->create(
            ActiveSync_TestCase::getTestDevice()
        );
    }

}
