<?php
/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     Tinebase
 * @subpackage  Group
 * @license     http://www.gnu.org/licenses/agpl.html
 * @copyright   Copyright (c) 2008-2015 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Guilherme Striquer Bisotto <guilherme.bisotto@serpro.gov.br>
 */

/**
 * Test class for Tinebase_Group
 */
class Custom_Tinebase_GroupTest extends Tinebase_GroupTest
{

    /**
     * Returns true if backend is marked as readonly
     * @return boolean
     */
    protected function isReadonlyBackend()
    {
        if(Tinebase_Group::getInstance() instanceof Tinebase_Group_Ldap) {
            $config = Tinebase_Config::getInstance();
            $userBackendConfig = $config->get(Tinebase_Config::USERBACKEND);
            if($userBackendConfig->readonly) {
                return true;
            }
        }

        return false;
    }

    /**
     * try to add a group
     *
     * @return Tinebase_Model_Group
     */
    public function testAddGroup()
    {
        if($this->isReadonlyBackend()) {
            $this->markTestSkipped("Backend is marked as readonly");
        }

        $randomGroupName = 'tine20phpunit' . Tinebase_Record_Abstract::generateUID();

        $newGroup = Tinebase_Group::getInstance()->addGroup(new Tinebase_Model_Group(array(
            'name'          => $randomGroupName,
            'description'   => 'Group from test testAddGroup'
        )));

        sleep(1);

        $group = Tinebase_Group::getInstance()->getGroupByName($randomGroupName);

        $this->assertEquals($newGroup->getId(), $group->getId());

        return $group;
    }

    /**
     * try to get all groups containing Managers in their name
     */
    public function testGetGroups()
    {
        if($this->isReadonlyBackend()) {
            $this->markTestSkipped("Backend is marked as readonly");
        }
    }

    /**
     * try to get the group with the name tine20phpunit
     *
     */
    public function testGetGroupByName()
    {
        if($this->isReadonlyBackend()) {
            $this->markTestSkipped("Backend is marked as readonly");
        }
    }

    /**
     * try to get a group by
     *
     */
    public function testGetGroupById()
    {
        $userBackendConfig = Tinebase_Config::getInstance()->get(Tinebase_Config::USERBACKEND);

        $adminGroup = Tinebase_Group::getInstance()->getGroupByName($userBackendConfig[Tinebase_User::DEFAULT_ADMIN_GROUP_NAME_KEY]);

        $group = Tinebase_Group::getInstance()->getGroupById($adminGroup->id);

        $this->assertEquals($adminGroup->id, $group->id);
    }

    /**
     * try to update a group
     *
     */
    public function testUpdateGroup()
    {
        if($this->isReadonlyBackend()) {
            $this->markTestSkipped("Backend is marked as readonly");
        }


        $testGroup = $this->testAddGroup();

        $newName = 'tine20phpunit' . Tinebase_Record_Abstract::generateUID();
        $newDescription = 'New Description of group from testUpdateGroup';

        $testGroup->name = $newName;
        $testGroup->description = $newDescription;

        $group = Tinebase_Group::getInstance()->updateGroup($testGroup);

        $this->assertEquals($testGroup->name, $group->name);
        $this->assertEquals($testGroup->description, $group->description);

    }

    /**
     * try to set/get group members
     *
     * @return Tinebase_Model_Group
     */
    public function testSetGroupMembers($testGroup = null, $testGroupMembersArray = null)
    {
        if($this->isReadonlyBackend()) {
            $this->markTestSkipped("Backend is marked as readonly");
        }
    }

    /**
     * try to add a group member
     */
    public function testAddGroupMember()
    {
        if($this->isReadonlyBackend()) {
            $this->markTestSkipped("Backend is marked as readonly");
        }
    }

    /**
     * try to remove a group member
     */
    public function testRemoveGroupMember()
    {
        if($this->isReadonlyBackend()) {
            $this->markTestSkipped("Backend is marked as readonly");
        }
    }

    /**
     * try to delete a group
     */
    public function testDeleteGroup()
    {
        if($this->isReadonlyBackend()) {
            $this->markTestSkipped("Backend is marked as readonly");
        }
    }

    /**
     * testSetGroupMemberships
     */
    public function testSetGroupMemberships()
    {
        if($this->isReadonlyBackend()) {
            $this->markTestSkipped("Backend is marked as readonly");
        }
    }

    /**
     * testSyncLists
     *
     * @see 0005768: create addressbook lists when migrating users
     *
     * @todo make this work for LDAP accounts backend: currently the user is not present in sync backend but in sql
     */
    public function testSyncLists()
    {
        if($this->isReadonlyBackend()) {
            $this->markTestSkipped("Backend is marked as readonly");
        }
    }

    /**
     * testRemoveAccountPrimaryGroup
     *
     * @see 0007384: deleting a group that is an accounts primary group fails
     */
    public function testRemoveAccountPrimaryGroup()
    {
        if($this->isReadonlyBackend()) {
            $this->markTestSkipped("Backend is marked as readonly");
        }

        $testGroup = $this->testAddGroup();
        $sclever = Tinebase_User::getInstance()->getFullUserByLoginName('sclever');
        $sclever->accountPrimaryGroup = $testGroup->getId();

        $groupBackend = Tinebase_Group::getInstance();
        if($groupBackend instanceof Tinebase_Group_Ldap) {
            $sqlGroupBackend = Tinebase_Group::factory(Tinebase_User::SQL);
            $sqlGroupBackend->addGroup($testGroup);
        }

        Tinebase_User::getInstance()->updateUser($sclever);

        Tinebase_Group::getInstance()->deleteGroups($testGroup);

        $sclever = Tinebase_User::getInstance()->getFullUserByLoginName('sclever');
        $this->assertEquals(Tinebase_Group::getInstance()->getDefaultGroup()->getId(), $sclever->accountPrimaryGroup);
    }

}