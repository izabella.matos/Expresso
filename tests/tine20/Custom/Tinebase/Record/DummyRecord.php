<?php
/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     Tinebase
 * @subpackage  Record
 * @license     http://www.gnu.org/licenses/agpl.html
 * @copyright   Copyright (c) 2015 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Emerson Faria Nobre <emerson-faria.nobre@serpro.gov.br>
 */

class Custom_Tinebase_Record_DummyRecord extends Tinebase_Record_DummyRecord
{
    /**
    * application
    *
    * @var string
    */
    protected $_application = 'Tinebase';
}
