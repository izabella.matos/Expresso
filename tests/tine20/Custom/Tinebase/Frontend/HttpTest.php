<?php
/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     Tinebase
 * @subpackage  Frontend
 * @license     http://www.gnu.org/licenses/agpl.html
 * @copyright   Copyright (c) 2016 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Marcelo Teixeira <marcelo.teixeira@serpro.gov.br>
 */

/**
 * Test helper
 */
require_once dirname(dirname(dirname(dirname(__FILE__)))) . DIRECTORY_SEPARATOR . 'TestHelper.php';

/**
 * Custom test class for Tinebase_Frontend_HttpTest
 *
 */
class Custom_Tinebase_Frontend_HttpTest extends Tinebase_Frontend_HttpTest
{
    /**
     * disable redirect in Tinebase_Http_UserAgent
     */
    public function testMainScreen()
    {
        Tinebase_Http_UserAgent::setDisabled();

        if (version_compare(PHPUnit_Runner_Version::id(), '3.3.0', '<')) {
            $this->markTestSkipped('phpunit version < 3.3.0 cant cope with headers');
        }
        ob_start();
        $this->_uit->mainScreen();
        $html = ob_get_clean();

        $this->assertGreaterThan(100, strlen($html));
    }
}
