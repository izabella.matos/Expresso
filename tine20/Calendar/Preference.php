<?php
/**
 * Tine 2.0
 * 
 * @package     Calendar
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Cornelius Weiss <c.weiss@metaways.de>
 * @copyright   Copyright (c) 2009-2010 Metaways Infosystems GmbH (http://www.metaways.de)
 */


/**
 * backend for Calendar preferences
 *
 * @package     Calendar
 */
class Calendar_Preference extends Tinebase_Preference_Abstract
{
    /**
     * where daysview should be scrolled to
     */
    const DAYSVIEW_STARTTIME = 'daysviewstarttime';
    /**
     * start time for daysview and weekview
     */
    const DAYSTIMEVIEW_STARTTIME = 'daystimeviewstarttime';
    /**
     * end time for daysview and weekview
     */
    const DAYSTIMEVIEW_ENDTIME = 'daystimeviewendtime';
    /**
     * Day granularity time for the day and week views
     */
    const DAYSTIMEVIEW_GRANULARITY  = 'daystimeviewgranularity';
    
    /**
     * default calendar all newly created/invited events are placed in
     */
    const DEFAULTCALENDAR = 'defaultCalendar';
    
    /**
     * have name of default favorite an a central palce
     * _("All my events")
     */
    const DEFAULTPERSISTENTFILTER_NAME = "All my events";
    
    /**
     * general notification level
     */
    const NOTIFICATION_LEVEL = 'notificationLevel';
    
    /**
     * send notifications of own updates
     */
    const SEND_NOTIFICATION_OF_OWN_ACTIONS = 'sendnotificationsofownactions';

    /**
     * send notifications of own updates
     */
    const SEND_NOTIFICATION_FOR_ATTENDEES = 'sendnotificationsforattendes';

    /**
     * enable default alarm 
     */
    const DEFAULTALARM_ENABLED = 'defaultalarmenabled';
    
    /**
     * default alarm time in minutes before
     */
    const DEFAULTALARM_MINUTESBEFORE = 'defaultalarmminutesbefore';

    /**
     * event sequence start
     */
    const EVENT_SEQUENCESTART = 'eventsequencestart';
    
    /**
     * work week days
     */
    const WORK_WEEK = 'workweekdays';

    /**
     * numbers of minicals to print
     */
    const MINICAL_PRINT = 'minicalprint';

    /**
     * size of the paper to print
     */
    const PRINT_PAPER_SIZE = 'papersize';

    /**
     * print annotations on day view print
     */
    const PRINT_ANNOTATIONS = 'printannotations';

    /** Constants to print paper formats (mm)
     * Change the numbers here , have to change in printer/base.js
     */
    const A3_SIZE_HEIGHT_PORTRAIT_PRINT          =  324;
    const A4_SIZE_HEIGHT_PORTRAIT_PRINT          =  223;
    const A5_SIZE_HEIGHT_PORTRAIT_PRINT          =  136;
    const A3_SIZE_HEIGHT_LANDESCAPE_PRINT          =  224;
    const A4_SIZE_HEIGHT_LANDESCAPE_PRINT          =  137;
    const A5_SIZE_HEIGHT_LANDESCAPE_PRINT          =  74;

    /**
     * @var string application
     */
    protected $_application = 'Calendar';
        
    /**
     * get all possible application prefs
     *
     * @return  array   all application prefs
     */
    public function getAllApplicationPreferences()
    {
        $allPrefs = array(
            self::DAYSVIEW_STARTTIME,
            self::DAYSTIMEVIEW_STARTTIME,
            self::DAYSTIMEVIEW_ENDTIME,
            self::DAYSTIMEVIEW_GRANULARITY,
            self::DEFAULTCALENDAR,
            self::DEFAULTPERSISTENTFILTER,
            self::NOTIFICATION_LEVEL,
            self::SEND_NOTIFICATION_OF_OWN_ACTIONS,
            self::SEND_NOTIFICATION_FOR_ATTENDEES,
            self::DEFAULTALARM_ENABLED,
            self::DEFAULTALARM_MINUTESBEFORE,
            self::EVENT_SEQUENCESTART,
            self::WORK_WEEK,
            self::MINICAL_PRINT,
            self::PRINT_PAPER_SIZE,
            self::PRINT_ANNOTATIONS,
        );
            
        return $allPrefs;
    }
    
    /**
     * get translated right descriptions
     * 
     * @return  array with translated descriptions for this applications preferences
     */
    public function getTranslatedPreferences()
    {
        $translate = Tinebase_Translation::getTranslation($this->_application);

        $prefDescriptions = array(
            self::DAYSVIEW_STARTTIME => array(
                'label'         => $translate->_('Start Time'),
                'description'   => $translate->_('Position on the left time axis, day and week view should start with'),
            ),
            self::DAYSTIMEVIEW_STARTTIME => array(
                'label'         => $translate->_('Day Start At'),
                'description'   => $translate->_('Start time that will be shown in Week and day sheet calendar view'),
            ),
            self::DAYSTIMEVIEW_ENDTIME => array(
                'label'         => $translate->_('Day End At'),
                'description'   => $translate->_('End time that will be shown in Week and day sheet calendar view'),
            ),
            self::DAYSTIMEVIEW_GRANULARITY => array(
                'label'         => $translate->_('Day hour granularity'),
                'description'   => $translate->_('Set the interval time between hours that is showing in day and week views'),
            ),
            self::DEFAULTCALENDAR  => array(
                'label'         => $translate->_('Default Calendar'),
                'description'   => $translate->_('The default calendar for invitations and new events'),
            ),
            self::DEFAULTPERSISTENTFILTER  => array(
                'label'         => $translate->_('Default Favorite'),
                'description'   => $translate->_('The default favorite which is loaded on calendar startup'),
            ),
            self::NOTIFICATION_LEVEL => array(
                'label'         => $translate->_('Get Notification Emails'),
                'description'   => $translate->_('The level of actions you want to be notified about. Please note that organizers will get notifications for all updates including attendee answers unless this preference is set to "Never"'),
            ),
            self::SEND_NOTIFICATION_FOR_ATTENDEES => array(
                'label'         => $translate->_('Send Notifications Emails for Attendees'),
                'description'   => $translate->_('Send Notifications Emails for Events That You Create/Change'),
            ),
            self::SEND_NOTIFICATION_OF_OWN_ACTIONS => array(
                'label'         => $translate->_('Send Notifications Emails of own Actions'),
                'description'   => $translate->_('Get notifications emails for actions you did yourself'),
            ),
            self::DEFAULTALARM_ENABLED => array(
                'label'         => $translate->_('Enable Standard Alarm'),
                'description'   => $translate->_('New events get a standard alarm as defined below'),
            ),
            self::DEFAULTALARM_MINUTESBEFORE => array(
                'label'         => $translate->_('Standard Alarm Time'),
                'description'   => $translate->_('Minutes before the event starts'),
            ),
            self::EVENT_SEQUENCESTART => array(
                'label'         => $translate->_('Sequence Start'),
                'description'   => $translate->_('Event Sequence Start Number'),
            ),
            self::WORK_WEEK => array(
                'label'         => $translate->_('Define Work week days'),
                'description'   => $translate->_('Define the days of the week that is a work day'),
            ),
            self::MINICAL_PRINT => array(
                'label'         => $translate->_('Number of Mini Calendars to Print'),
                'description'   => $translate->_('Define how many mini calendars to print'),
            ),
            self::PRINT_PAPER_SIZE => array(
                'label'         => $translate->_('Paper Size to Print Calendar'),
                'description'   => $translate->_('Define the size of the paper to use when print calendar'),
            ),
            self::PRINT_ANNOTATIONS => array(
                'label'         => $translate->_('Print day view with annotations'),
                'description'   => $translate->_('Print annotations when print day sheet view'),
            ),
        );
        
        return $prefDescriptions;
    }
    
    /**
     * get preference defaults if no default is found in the database
     *
     * @param string $_preferenceName
     * @param string|Tinebase_Model_User $_accountId
     * @param string $_accountType
     * @return Tinebase_Model_Preference
     */
    public function getApplicationPreferenceDefaults($_preferenceName, $_accountId = NULL, $_accountType = Tinebase_Acl_Rights::ACCOUNT_TYPE_USER)
    {
        $preference = $this->_getDefaultBasePreference($_preferenceName);
        
        switch($_preferenceName) {
            case self::DAYSVIEW_STARTTIME:
                $doc = new DomDocument('1.0');
                $options = $doc->createElement('options');
                $doc->appendChild($options);
                
                $time = new Tinebase_DateTime('@0');
                for ($i=0; $i<48; $i++) {
                    $time->addMinute($i ? 30 : 0);
                    $timeString = $time->format('H:i');
                    
                    $value  = $doc->createElement('value');
                    $value->appendChild($doc->createTextNode($timeString));
                    $label  = $doc->createElement('label');
                    $label->appendChild($doc->createTextNode($timeString)); // @todo l10n
                    
                    $option = $doc->createElement('option');
                    $option->appendChild($value);
                    $option->appendChild($label);
                    $options->appendChild($option);
                }
                
                $preference->value      = '08:00';
                $preference->options = $doc->saveXML();
                break;
            case self::DAYSTIMEVIEW_STARTTIME:
                $doc = new DomDocument('1.0');
                $options = $doc->createElement('options');
                $doc->appendChild($options);

                $time = new Tinebase_DateTime('@0');
                for ($i=0; $i<24; $i++) {
                    $time->addMinute($i ? 60 : 0);
                    $timeString = $time->format('H'). 'hs';

                    $value  = $doc->createElement('value');
                    $value->appendChild($doc->createTextNode($timeString));
                    $label  = $doc->createElement('label');
                    $label->appendChild($doc->createTextNode($timeString)); // @todo l10n

                    $option = $doc->createElement('option');
                    $option->appendChild($value);
                    $option->appendChild($label);
                    $options->appendChild($option);
                }

                $preference->value      = '00hs';
                $preference->options = $doc->saveXML();
                break;
            case self::DAYSTIMEVIEW_ENDTIME:
                $doc = new DomDocument('1.0');
                $options = $doc->createElement('options');
                $doc->appendChild($options);

                $time = new Tinebase_DateTime('@0');
                for ($i=0; $i<24; $i++) {
                $time->addMinute($i ? 60 : 0);
                $timeString = $time->format('H'). 'hs';

                    $value  = $doc->createElement('value');
                    $value->appendChild($doc->createTextNode($timeString));
                    $label  = $doc->createElement('label');
                    $label->appendChild($doc->createTextNode($timeString)); // @todo l10n

                    $option = $doc->createElement('option');
                    $option->appendChild($value);
                    $option->appendChild($label);
                    $options->appendChild($option);
                }
                $value  = $doc->createElement('value');
                $value->appendChild($doc->createTextNode('24hs'));
                $label  = $doc->createElement('label');
                $label->appendChild($doc->createTextNode('24hs')); // @todo l10n

                $option = $doc->createElement('option');
                $option->appendChild($value);
                $option->appendChild($label);
                $options->appendChild($option);


                $preference->value      = '24hs';
                $preference->options = $doc->saveXML();
                break;
            case self::DAYSTIMEVIEW_GRANULARITY:
                $doc = new DomDocument('1.0');
                $options = $doc->createElement('options');
                $doc->appendChild($options);

                $time = new Tinebase_DateTime('@0');
                for ($i=1; $i<12; $i++) {
                $time->addMinute(5);
                $timeString = $time->format('i') . 'm';

                    $value  = $doc->createElement('value');
                    $value->appendChild($doc->createTextNode($timeString));
                    $label  = $doc->createElement('label');
                    $label->appendChild($doc->createTextNode($timeString)); // @todo l10n

                    $option = $doc->createElement('option');
                    $option->appendChild($value);
                    $option->appendChild($label);
                    $options->appendChild($option);
                }
                $value  = $doc->createElement('value');
                $value->appendChild($doc->createTextNode('60m'));
                $label  = $doc->createElement('label');
                $label->appendChild($doc->createTextNode('60m')); // @todo l10n

                $option = $doc->createElement('option');
                $option->appendChild($value);
                $option->appendChild($label);
                $options->appendChild($option);
                $preference->value      = '30m';
                $preference->options = $doc->saveXML();
                break;
            case self::DEFAULTCALENDAR:
                $this->_getDefaultContainerPreferenceDefaults($preference, $_accountId);
                break;
            case self::DEFAULTPERSISTENTFILTER:
                $preference->value          = Tinebase_PersistentFilter::getPreferenceValues('Calendar', $_accountId, "All my events");
                break;
            case self::NOTIFICATION_LEVEL:
                $translate = Tinebase_Translation::getTranslation($this->_application);
                // need to put the translations strings here because they were not found in the xml below :/
                // _('Never') _('On invitation and cancellation only') _('On time changes') _('On all updates but attendee responses') _('On attendee responses too')
                $preference->value      = Calendar_Controller_EventNotifications::NOTIFICATION_LEVEL_EVENT_RESCHEDULE;
                $preference->options    = '<?xml version="1.0" encoding="UTF-8"?>
                    <options>
                        <option>
                            <value>'. Calendar_Controller_EventNotifications::NOTIFICATION_LEVEL_NONE . '</value>
                            <label>'. $translate->_('Never') . '</label>
                        </option>
                        <option>
                            <value>'. Calendar_Controller_EventNotifications::NOTIFICATION_LEVEL_INVITE_CANCEL . '</value>
                            <label>'. $translate->_('On invitation and cancellation only') . '</label>
                        </option>
                        <option>
                            <value>'. Calendar_Controller_EventNotifications::NOTIFICATION_LEVEL_EVENT_RESCHEDULE . '</value>
                            <label>'. $translate->_('On time/location changes') . '</label>
                        </option>
                        <option>
                            <value>'. Calendar_Controller_EventNotifications::NOTIFICATION_LEVEL_EVENT_UPDATE . '</value>
                            <label>'. $translate->_('On all updates but attendee responses') . '</label>
                        </option>
                        <option>
                            <value>'. Calendar_Controller_EventNotifications::NOTIFICATION_LEVEL_ATTENDEE_STATUS_UPDATE . '</value>
                            <label>'. $translate->_('On attendee responses too') . '</label>
                        </option>
                    </options>';
                break;
            case self::SEND_NOTIFICATION_FOR_ATTENDEES:
                $translate = Tinebase_Translation::getTranslation($this->_application);
                $preference->value      = Calendar_Controller_EventNotifications::NOTIFICATION_LEVEL_ATTENDEES_ASK;
                $preference->options = '<?xml version="1.0" encoding="UTF-8"?>
                <options>
                       <option>
                       <value>'. Calendar_Controller_EventNotifications::NOTIFICATION_LEVEL_ATTENDEES_NONE .'</value>
                       <label>'. $translate->_('Never') . '</label>
                       </option>
                       <option>
                       <value>'. Calendar_Controller_EventNotifications::NOTIFICATION_LEVEL_ATTENDEES_ASK . '</value>
                       <label>'. $translate->_('Ask me before send notification') . '</label>
                       </option>
                       <option>
                       <value>'. Calendar_Controller_EventNotifications::NOTIFICATION_LEVEL_ATTENDEES_AUTOMATIC . '</value>
                       <label>'. $translate->_('Send notification automatically') . '</label>
                       </option>
                </options>';
                break;
            case self::SEND_NOTIFICATION_OF_OWN_ACTIONS:
                $preference->value      = 0;
                $preference->options    = '<?xml version="1.0" encoding="UTF-8"?>
                    <options>
                        <special>' . Tinebase_Preference_Abstract::YES_NO_OPTIONS . '</special>
                    </options>';
                break;
            case self::DEFAULTALARM_ENABLED:
                $preference->value      = 0;
                $preference->options    = '<?xml version="1.0" encoding="UTF-8"?>
                    <options>
                        <special>' . Tinebase_Preference_Abstract::YES_NO_OPTIONS . '</special>
                    </options>';
                break;
            case self::DEFAULTALARM_MINUTESBEFORE:
                $preference->value      = 15;
                $preference->options    = '';
                break;
            case self::EVENT_SEQUENCESTART:
                $preference->value      = 0;
                $preference->type       = Tinebase_Model_Preference::TYPE_FORCED;
                $preference->options    = '<?xml version="1.0" encoding="UTF-8"?>
                    <options>
                        <option>
                            <value>0</value>
                            <label>0</label>
                        </option>
                        <option>
                            <value>1</value>
                            <label>1</label>
                        </option>
                    </options>';
                break;
            case self::WORK_WEEK:
                $preference->isweek      = true;
                $preference->options    = '';
                break;
            case self::MINICAL_PRINT:
                $doc = new DomDocument('1.0');
                $options = $doc->createElement('options');
                $doc->appendChild($options);
                for ($i=1; $i<4; $i++) {
                    $value  = $doc->createElement('value');
                    $value->appendChild($doc->createTextNode($i));
                    $label  = $doc->createElement('label');
                    $label->appendChild($doc->createTextNode($i)); // @todo l10n

                    $option = $doc->createElement('option');
                    $option->appendChild($value);
                    $option->appendChild($label);
                    $options->appendChild($option);
                }

                $preference->value      = '3';
                $preference->options = $doc->saveXML();
                break;
            case self::PRINT_PAPER_SIZE:
                $translate = Tinebase_Translation::getTranslation($this->_application);
                $preference->value      = self::A4_SIZE_HEIGHT_PORTRAIT_PRINT;
                $preference->options = '<?xml version="1.0" encoding="UTF-8"?>
                <options>
                       <option>
                       <value>'. self::A3_SIZE_HEIGHT_PORTRAIT_PRINT .'</value>
                       <label>'. $translate->_('A3 (Portrait)') . '</label>
                       </option>
                       <option>
                       <value>'. self::A4_SIZE_HEIGHT_PORTRAIT_PRINT . '</value>
                       <label>'. $translate->_('A4 (Portrait)') . '</label>
                       </option>
                       <option>
                       <value>'. self::A5_SIZE_HEIGHT_PORTRAIT_PRINT . '</value>
                       <label>'. $translate->_('A5 (Portrait)') . '</label>
                       </option>
                       <option>
                       <value>'. self::A3_SIZE_HEIGHT_LANDESCAPE_PRINT . '</value>
                       <label>'. $translate->_('A3 (Landscape)') . '</label>
                       </option>
                       <option>
                       <value>'. self::A4_SIZE_HEIGHT_LANDESCAPE_PRINT . '</value>
                       <label>'. $translate->_('A4 (Landscape)') . '</label>
                       </option>
                       <option>
                       <value>'. self::A5_SIZE_HEIGHT_LANDESCAPE_PRINT . '</value>
                       <label>'. $translate->_('A5 (Landscape)') . '</label>
                       </option>
                </options>';
                break;
            case self::PRINT_ANNOTATIONS:
                $preference->value      = 0;
                $preference->options    = '<?xml version="1.0" encoding="UTF-8"?>
                    <options>
                        <special>' . Tinebase_Preference_Abstract::YES_NO_OPTIONS . '</special>
                    </options>';
                break;
           default:
                throw new Tinebase_Exception_NotFound('Default preference with name ' . $_preferenceName . ' not found.');
        }
        
        return $preference;
    }
}
