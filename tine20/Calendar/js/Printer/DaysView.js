Tine.Calendar.Printer.DaysViewRenderer = Ext.extend(Tine.Calendar.Printer.BaseRenderer, {
    paperHeight: 223,
    generateBody: function(view) {
        var mesh = [];
        y=1;
        for(var i = 0; i < 7; i++){
            var d = (view.startDay+i)%7;
            g = view.work & y;
            if(g > 0) {
                var weekday = view.startDate.add(Date.DAY, i);
                mesh.push(weekday);
            }
            y <<= 1;
        }
        workdays=0;
        y=1;
        for(var j=0;j < 7 ; j++){
            g = view.work & y;
            if(g > 0) {
                workdays++;
            }
            y <<= 1;
        }

        var daysHtml = this.splitDays(view.store, mesh, workdays),
        body = [];
        body.push(this.getPageSize());
        var allDayData = '';
        var scrollerData = '';
        body.push(this.generateTitle(view));
        view.store.each(function(event) {
            if(event.get('is_all_day_event')){
                view.numberWholeDayEvents++;
            }
        }, this);

        if (view.numOfDays === 1) {
            this.paperHeight = Math.round(this.paperHeight - ((view.numberWholeDayEvents * 18)/3.779527));
            view.printConvert = (this.paperHeight * 3.779527)/864;
            var singleDay = this.generateDayData(view);
            numAllDayEvents = 0;
            for(var i=0; i<singleDay.length; i++) {
                if(singleDay[i]["allDay"]) {
                    allDayData += singleDay[i]["allDay"];
                    numAllDayEvents++;
                } else {
                    scrollerData += singleDay[i]["scroller"];
                }
            }
            var height = 18 * numAllDayEvents;
            body.push(String.format('<div class="x-panel-body x-panel-body-noheader x-panel-body-noborder" id="ext-gen594" style="width: 100%; height: 96px;">' +
                '<div id="ext-comp-1273" style="width: 100%; height: 96px;"><div hidefocus="true" class="cal-daysviewpanel">' +
                '<div class="cal-daysviewpanel-viewport" id="ext-gen624">{0}{1}</div></div</div></div>',this.generateDayHeader(view,allDayData,height),this.generateDayCalRows(scrollerData,view)));
        } else if (view.numOfDays < 9) {
            if (view.numOfDays == 7 && view.startDate.format('w') == 1) {
                // iso week
                body.push(this.generateIsoWeek(daysHtml));
            } else {
                body.push(String.format('<table class="cal-print-weekview">{0}</table>', this.generateCalRows(daysHtml, 2, true)));
            }
        } else {
            body.push(String.format('<tableclass="cal-print-weekview">{0}</table>', this.generateCalRows(daysHtml, 2, true)));
        }
        
        return body.join("\n");
    },
    
    getTitle: function(view) {
        if (view.numOfDays == 1) {
            return String.format(view.dayFormatString + ' {3}', view.startDate.format('l'), view.startDate.format('j'), view.startDate.format('F'), view.startDate.format('Y'));
        } else {
            var endDate = view.startDate.add(Date.DAY, view.numOfDays -1),
                startDayOfMonth = view.startDate.format('j. '),
                startMonth = view.startDate.format('F '),
                startYear = view.startDate.format('Y '),
                endDayOfMonth = endDate.format('j. '),
                endMonth = endDate.format('F '),
                endYear = endDate.format('Y '),
                week = view.numOfDays == 7 ? String.format(view.app.i18n._('Week {0} :'), view.startDate.add(Date.DAY, 1).getWeekOfYear()) + ' ' : '';
                
                if (startYear === endYear) startYear = '';
                if (startMonth === endMonth) startMonth = '';
                
                return week + startDayOfMonth + startMonth + startYear + ' - ' + endDayOfMonth + endMonth + endYear;
        }
    },
  
    generateIsoWeek: function(daysHtml) {
        var height = this.paperHeight/4;
        return ['<table>',
            '<tr style="height: ' + height + 'mm;">',
                '<td class="cal-print-daycell" width="50%">', daysHtml[0], '</td>',
                '<td class="cal-print-daycell" width="50%">', daysHtml[1], '</td>',
            '</tr>', 
            '<tr style="height: ' + height + 'mm;">',
                '<td class="cal-print-daycell" width="50%">', daysHtml[2], '</td>',
                '<td class="cal-print-daycell" width="50%">', daysHtml[3], '</td>',
            '</tr>',
            '<tr style="height: ' + height + 'mm;">',
                '<td class="cal-print-daycell" width="50%">', daysHtml[4], '</td>',
                '<td class="cal-print-daycell" width="50%">',
                    '<table style="padding: 0;">',
                        '<tr style="height: ' + height/2 + 'mm;">',
                            '<td class="cal-print-daycell" width="100%" style="padding: 0;">', daysHtml[5], '</td>',
                        '</tr>',
                        '<tr style="height: ' + height/2 + 'mm;">',
                            '<td class="cal-print-daycell" width="100%" style="padding: 0;">', daysHtml[6], '</td>',
                        '</tr>', 
                    '</table>',
            '</tr>', 
        '</table>'].join("\n");
    }
});
