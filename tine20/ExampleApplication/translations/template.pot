msgid ""
msgstr ""
"Project-Id-Version: Tine 2.0 - ExampleApplication\n"
"POT-Creation-Date: 2008-05-17 22:12+0100\n"
"PO-Revision-Date: 2008-07-29 21:14+0100\n"
"Last-Translator: Cornelius Weiss <c.weiss@metaways.de>\n"
"Language-Team: Tine 2.0 Translators\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: en\n"
"X-Poedit-Country: GB\n"
"X-Poedit-SourceCharset: utf-8\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: Controller.php:79
#, python-format
msgid "%s's personal example records"
msgstr ""

#: Model/ExampleRecord.php:35
msgid "example record"
msgstr ""

#: Model/ExampleRecord.php:36
msgid "example records"
msgstr ""

#: Model/ExampleRecord.php:38
msgid "example record list"
msgstr ""

#: Model/ExampleRecord.php:39
msgid "example record lists"
msgstr ""

#: Model/ExampleRecord.php:59 js/ExampleRecordEditDialog.js:70
#: js/ExampleRecordDetailsPanel.js:74
msgid "Name"
msgstr ""

#: Model/ExampleRecord.php:63 js/ExampleRecordEditDialog.js:76
#: js/ExampleRecordDetailsPanel.js:78
msgid "Status"
msgstr ""

#: Config.php:31
msgid "Status Available"
msgstr ""

#: Config.php:33
msgid ""
"Possible status. Please note that additional status might impact other "
"ExampleApplication systems on export or syncronisation."
msgstr ""

#: Setup/setup.xml:4
msgid "ExampleApplication"
msgstr ""

#: Setup/Initialize.php:33
msgid "Completed"
msgstr ""

#: Setup/Initialize.php:34
msgid "Cancelled"
msgstr ""

#: Setup/Initialize.php:35
msgid "In process"
msgstr ""

#: Preference.php:60
msgid "Default Example Record Container"
msgstr ""

#: Preference.php:61
msgid "The default container for new records"
msgstr ""

#: js/ExampleRecordEditDialog.js:53
msgid "ExampleRecord"
msgstr ""

#: js/ExampleRecordDetailsPanel.js:56
msgid "Total Example Records"
msgstr ""
