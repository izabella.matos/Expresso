<?php
/**
 * Tine 2.0
 *
 * @package     Tinebase_Shard
 * @subpackage  Log
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2015 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Emerson Faria Nobre <emerson-faria.nobre@serpro.gov.br>
 *
 */

/*
 * @package     Tinebase
 * @subpackage  Shard_Log
 */
class Tinebase_Shard_Log_Formatter extends Zend_Log_Formatter_Simple
{
    /**
     * session id
     *
     * @var string
     */
    protected static $_prefix;

    /**
     * username
     *
     * @var string
     */
    protected static $_username = NULL;

    /**
     * search strings
     *
     * @var array
     */
    protected $_search = array();

    /**
     * replacement strings
     *
     * @var array
     */
    protected $_replace = array();

}
