/*!
 * Ext JS Library 3.4.0
 * Copyright(c) 2006-2011 Sencha Inc.
 * licensing@sencha.com
 * http://www.sencha.com/license
 */
Ext.ns('Tine.Tinebase');
/**
 * @class Tine.Tinebase.WindowsScrollerMenu
 * @extends Object
 * Plugin (ptype = 'windowscrollermenu') for adding a tab scroller menu to tabs.
 * @constructor
 * @param {Object} config Configuration options
 * @ptype windowscrollermenu
 */
Tine.Tinebase.WindowScrollerMenu =  Ext.extend(Object, {
    /**
     * @cfg {Number} maxText How long should the title of each {@link Ext.menu.Item} be.
     */
    maxText        : 10,
    constructor    : function(config) {
        config = config || {};
        Ext.apply(this, config);
    },
    //private
    init : function(tabPanel) {
        Ext.apply(tabPanel, this.parentOverrides);

        tabPanel.windowScrollerMenu = this;
        var thisRef = this;

        tabPanel.on({
            render : {
                scope  : tabPanel,
                single : true,
                fn     : function() {
                    var newFn = tabPanel.createScrollers.createSequence(thisRef.createPanelsMenu, this);
                    tabPanel.createScrollers = newFn;
                }
            }
        });
    },

    // private && sequeneced
    createPanelsMenu : function() {
        var h = this.stripWrap.dom.offsetHeight;

        //move the right menu item to the left 18px
        var rtScrBtn = this.header.dom.firstChild;
        Ext.fly(rtScrBtn).applyStyles({
            right : '18px'
        });

        var stripWrap = Ext.get(this.strip.dom.parentNode);
        stripWrap.applyStyles({
             'margin-right' : '36px'
        });

        // Add the new righthand menu
        var scrollMenu = this.header.insertFirst({
            cls:'x-tab-tabmenu-right'
        });
        scrollMenu.setHeight(h);
        scrollMenu.addClassOnOver('x-tab-tabmenu-over');
        scrollMenu.on('click', this.showTabsMenu, this);

        this.scrollLeft.show = this.scrollLeft.show.createSequence(function() {
            scrollMenu.show();
        });

        this.scrollLeft.hide = this.scrollLeft.hide.createSequence(function() {
            scrollMenu.hide();
        });

    },

    /**
     * Returns the current maxText length;
     * @return {Number} this.maxText The current max text length.
     */
    getMaxText : function() {
        return this.maxText;
    },

    /**
     * Sets the maximum text size for each menu item.
     * @param {Number} t The max text per each menu item.
     */
    setMaxText : function(t) {
        this.maxText = t;
    },

    // private && applied to the tab panel itself.
    parentOverrides : {
        // all execute within the scope of the tab panel
        // private
        showTabsMenu : function(e) {
            if  (this.tabsMenu) {
                this.tabsMenu.destroy();
                this.un('destroy', this.tabsMenu.destroy, this.tabsMenu);
                this.tabsMenu = null;
            }
            this.tabsMenu =  new Ext.menu.Menu();
            this.on('destroy', this.tabsMenu.destroy, this.tabsMenu);

            this.generateTabMenuItems();

            var target = Ext.get(e.getTarget());
            var xy     = target.getXY();

            //Y param + 24 pixels
            xy[1] += 24;

            this.tabsMenu.showAt(xy);
        },

        // private
        generateTabMenuItems : function() {
            var curActive  = this.getActiveTab();
            var totalItems = this.items.getCount();

            this.items.each(function(item) {
                this.tabsMenu.add(this.autoGenMenuItem(item));
            }, this);
        },

        // private
        autoGenMenuItem : function(item) {
            var maxText = this.windowScrollerMenu.getMaxText();
            var text    = Ext.util.Format.ellipsis(item.title, maxText);

            return {
                text      : text,
                handler   : this.showTabFromMenu,
                scope     : this,
                disabled  : item.disabled,
                tabToShow : item,
                iconCls   : item.iconCls
            }
        },

        // private
        showTabFromMenu : function(menuItem) {
            var win = Ext.getCmp(menuItem.tabToShow.ctid);
            if (win) {
                win.show();
                win.restore();
                this.remove(menuItem.tabToShow);
            }
        }
    }
});

Ext.reg('windowscrollermenu', Tine.Tinebase.WindowScrollerMenu);