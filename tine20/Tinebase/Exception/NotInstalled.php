<?php
/**
 * Tine 2.0
 *
 * @package     Tinebase
 * @subpackage  Exception
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2007-2015 Metaways Infosystems GmbH (http://www.metaways.de)
 * @copyright   Copyright (c) 2015 Serpro (http://www.serpro.gov.br)
 * @author      Flávio Gomes da Silva Lisboa <flavio.lisboa@serpro.gov.br>
 *
 */

/**
 * Exception for not installed application
 *
 * @package     Tinebase
 * @subpackage  Exception
 */
class Tinebase_Exception_NotInstalled extends Tinebase_Exception
{
    /**
    * the constructor
    *
    * @param string $_message
    * @param int $_code (default: 503 Service Unavailable)
    */
    public function __construct($_message, $_code = 503)
    {
        parent::__construct($_message, $_code);
    }
}
