<?php
/**
 * Tine 2.0
 * 
 * @package     Tinebase
 * @subpackage  Exception
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2007-2008 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Guilherme Striquer Bisotto <guilherme.bisotto@serpro.gov.br>
 *
 */

/**
 * NotFound exception
 * 
 * @package     Tinebase
 * @subpackage  Exception
 */
class Tinebase_Exception_ContainerNotFound extends Tinebase_Exception_NotFound
{
    public function __construct($_message, $_code=405)
    {
        parent::__construct($_message, $_code);
    }
}
