<?php
/**
 * Tine 2.0
 * 
 * @package     AppLauncher
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Jonas Fischer <j.fischer@metaways.de>
 * @copyright   Copyright (c) 2008-2011 Metaways Infosystems GmbH (http://www.metaways.de)
 *
 */

/**
 * class for AppLauncher initialization
 * 
 * @package     Setup
 */
class AppLauncher_Setup_Initialize extends Setup_Initialize
{
    // nothing to do here
    // Intentionally left blank
}
