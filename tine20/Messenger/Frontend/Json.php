<?php
/**
*
* This class handles all Json requests for the application
*
* @package     Clients
* @subpackage  Frontend
*/
class Messenger_Frontend_Json extends Tinebase_Frontend_Json_Abstract
{    
    
    const LOG_FILE = '/tmp/messenger/filetransfer.log';

    /**
    * controller
    *
    * @var Controller_Client
    */
   protected $_controller = NULL;

   /**
    * the constructor
    *
    */
   public function __construct()
   {
       $this->_applicationName = 'Messenger';
       $this->_controller = Messenger_Controller::getInstance();
   }
   
   public function getRegistryData()
   {
       return $this->_controller->getSettings();
   }
   
   /**
    *  
    */
   public function getLocalServerInfo($login)
   {
       return $this->_controller->getLocalServerInfo($login);
   }
   
   public function removeTempFiles($files)
   {
       return $this->_controller->removeTempFiles($files);
   }
   
   public function saveChatHistory($id, $title, $content)
   {
       return $this->_controller->saveChatHistory($id, $title, $content);
   }
   
   public function saveHistory($jid, $contact, $direction, $message, $time)
   {
       return $this->_controller->saveHistory($jid, $contact, $direction, $message, $time);
   }
   
   public function listHistory($jid, $contact)
   {
       return $this->_controller->listHistory($jid, $contact);
   }

   public function getHistory($jid, $contact, $date)
   {
       return $this->_controller->getHistory($jid, $contact, $date);
   }
   
   public function getHtmlHistory($jid, $contact, $nick, $date)
   {
        $response = null;
        $history = $this->_controller->getHtmlHistory($jid, $contact, $nick, $date);
        if ($history == '') {
            $response = array(
                'status' => 'false',
                'content' => ''
            );
        } else {
            $response = array(
                'status' => 'true',
                'content' => $history
            );
        }
       return $response;
   }

   public function logFileTransfer($text)
   {
       //Tinebase_Core::getLogger()->info($text);
       $bytes = file_put_contents(self::LOG_FILE, date('Y-m-d h:i:s') . '   ' . $text . PHP_EOL, FILE_APPEND);
       
       return $bytes !== false ?
           array('log' => $text) :
           array('log' => 'ERROR ON SAVING FILE');
   }
   
   /**
     * joins all given tempfiles in given order to a single new tempFile
     *
     * @param array of tempfiles arrays $tempFiles
     * @return array new tempFile
     */
    public function joinTempFiles($tempFilesData)
    {
        $tempFileRecords = new Tinebase_Record_RecordSet('Tinebase_Model_TempFile');
        foreach($tempFilesData as $tempFileData) {
            $record = new Tinebase_Model_TempFile(array(), TRUE);
            $record->setFromJsonInUsersTimezone($tempFileData);
            $tempFileRecords->addRecord($record);
        }

        $joinedTempFile = Messenger_TempFile::getInstance()->joinTempFiles($tempFileRecords);

        return $joinedTempFile->toArray();
    }

    /**
     * Save settings
     * @param array $_data
     * @return array
     */
    public function saveSettings($_data)
    {
        $data = array(
            Messenger_Config::DOMAIN => $_data[Messenger_Config::DOMAIN],
            Messenger_Config::RESOURCE => $_data[Messenger_Config::RESOURCE],
            Messenger_Config::FORMAT => $_data[Messenger_Config::FORMAT],
            Messenger_Config::SERVER_URL => $_data[Messenger_Config::SERVER_URL],
            Messenger_Config::TEMP_FILES => $_data[Messenger_Config::TEMP_FILES],
            Messenger_Config::DISABLE_VIDEO_CHAT => $_data[Messenger_Config::DISABLE_VIDEO_CHAT]
        );

        $result = $this->_controller->saveSettings($data);
        return array(
            'status' => $result ? 'success' : 'failure'
        );
    }

    /**
     * Get settings
     * @return array
     */
    public function getSettings()
    {
        return $this->_controller->getSettings();
    }
    
    public function searchContacts($name, $start, $limit)
    {
        $filter = array(
            array(
                'field'     => 'query',
                'operator'  => 'contains',
                'value'     => $name,
                'id'        => 'quickFilter'
            )
        );
        $paging = array(
            'sort'  => 'full_name',
            'start' => $start,
            'limit' => $limit
        );

        return $this->searchAccountRecords($filter, $paging);
    }
    
    public function searchAccountRecords($filter, $paging)
    {
        return $this->_search($filter, $paging, Messenger_Controller_AccountRecord::getInstance(), 'Messenger_Model_AccountRecordFilter');
    }
    
    public function getCustomNameFromID($accountID)
    {
        $filter = array(
            array(
                'field' => 'query',
                'operator' => 'contains',
                'value' => $accountID,
                'id' => 'quickFilter'
            ),
            array(
                'field' => 'name',
                'operator' => 'equals',
                'value' => 'name'
            )
        );

        return $this->_search($filter, NULL, Messenger_Controller_PreferenceRecord::getInstance(), 'Messenger_Model_PreferenceRecordFilter');
    }
   
}