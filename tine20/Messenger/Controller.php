<?php

class Messenger_Controller extends Tinebase_Controller_Event
{

    /*
     * path to save chat history temporarily
     */
    const TMP_PATH = '/tmp/tine20im';

    /*
     * path to save contact history
     */
    const HISTORY_PATH = '/var/tmp/apache2/tine20im/history';

    /**
     * holds the instance of the singleton
     *
     * @var Messenger_Controller
     */
    private static $_instance = NULL;
    
    /**
     * application name
     *
     * @var string
     */
    protected $_applicationName = 'Messenger';

    /**
     * constructor (get current user)
     */
    private function __construct()
    {
    }
    
    /**
     * don't clone. Use the singleton.
     *
     */
    private function __clone() 
    {        
    }
    
    /**
     * the singleton pattern
     *
     * @return Messenger_Controller
     */
    public static function getInstance() 
    {
        if (self::$_instance === NULL) {
            self::$_instance = new Messenger_Controller;
        }
        
        return self::$_instance;
    }
    

    /**
     *
     * @return JSON_Array
     */
    public function getLocalServerInfo($_login)
    {
        return array(
            'ip' => $_SERVER['SERVER_ADDR']
        );
    }
    
    public function removeTempFiles($_files)
    {
        $value = true;
        foreach ($_files as $file)
            $value = $value && unlink($file);
        
        return array(
            'status' => $value
        );
    }
    
    public function listHistory($jid, $contact)
    {
        $response = null;
        $path = self::HISTORY_PATH . '/' . $jid . '/' . $contact;
        if (!file_exists($path)) {
            $response = array(
                'status'  => false,
                'content' => ''
            );
        } else {
            $dir = new DirectoryIterator($path);
            $dates = array();
            foreach ($dir as $file)
            {
                if (!$file->isDot())
                    $dates[] = $file->getBasename('.json');
            }
            
            rsort($dates);
            
            $response = array(
                'status'  => true,
                'content' => $dates
            );
        }
        return $response;
    }
    
    public function getHistory($jid, $contact, $date)
    {
        $response = null;
        $filename = self::HISTORY_PATH . '/' . $jid . '/' . $contact . '/' . $date . '.json';
        if (!file_exists($filename)) {
            $response = array(
                'status' => 'false',
                'content' => ''
            );
        } else {
            $lines = explode("\n", file_get_contents($filename));
            $content = array();
            foreach ($lines as $line)
                $content[] = $line;
            
            $response = array(
                'status' => 'true',
                'content' => $content
            );
        }
        
        return $response;
    }

    public function getHtmlFormattedHistory($nick, $contents)
    {
        $history = $history . '<head><meta charset="UTF-8"></head>';
        $history = $history . '<body>';
        $history = $history . '<table>';

        $lines = explode("\n", $contents);
        foreach ($lines as $line)
        {
            $chatLine = json_decode($line);

            if ($chatLine->time == NULL)
                break;

            $color = $chatLine->dir == to ? '#8fb1e8' : '#98d96c';

            $history = $history . '<tr style="background-color:' . (($chatLine->dir == 'to') ? '#dfe8f6' : '#f8f8ff') . '">';
            $history = $history . '<td style="padding:4px;">';

            $history = $history . '<div style="font:13px verdana,arial; text-align:center; vertical-align:middle; font-weight: bold; margin-right: 10px; color: ' . $color . ';">';
            $history = $history . (($chatLine->dir == 'to') ? ($nick) : (Tinebase_Translation::getTranslation('Messenger')->_('ME')));
            $history = $history . '</div>';
            $history = $history . '<div style="font:13px helvetica,arial; font-weight: normal; color: grey; text-align:center; vertical-align:middle;">(';
            $history = $history . $chatLine->time . ')</div>';
            $history = $history . '</div>';

            $history = $history . '</td>';
            $history = $history . '<td style="padding:4px;">';

            $history = $history . '<div style="float: left; font:13px arial">';
            $history = $history . $chatLine->msg;
            $history = $history . '</div>';

            $history = $history . '</td>';
            $history = $history . '</tr>';
        }
        $history = $history . '</table>';
        $history = $history . '</body>';

        return $history;

    }

    public function getHtmlHistory($jid, $contact, $nick, $date)
    {
        $filename = $date . '.json';
        $file = Messenger_Controller::HISTORY_PATH . '/' . $jid . '/' . $contact . '/' . $filename;
        $history = '';

        if (file_exists($file)) {
            $filename = str_replace('@', '_', $jid) . '--' . str_replace('@', '_', $contact) . '--' . $date . '.html';

            header('Content-Description: File Transfer');
            header('Cache-Control: private, max-age=0');
            header("Expires: -1");
            header("Pragma: cache");
            header('Content-Disposition: attachment; filename="' . $filename . '"');

            $history = $this->getHtmlFormattedHistory($nick, file_get_contents($file));
        }

        return $history;
    }

    public function saveHistory($jid, $contact, $message)
    {
        $path = self::HISTORY_PATH . '/' . $jid . '/' . $contact . '/';
        if (!file_exists($path))
            mkdir($path, 0777, true);

        $filename = date('Y-m-d') . '.json';

        $result = file_put_contents($path . $filename, $message . PHP_EOL, FILE_APPEND);

        return array('status' => $result, 'what' => 'history', 'file' => $path . $filename);
    }
    
    public function saveChatHistory($id, $title, $content)
    {
        $config = self::getSettings();
        
        $tempPath = !empty($config['tempFiles']) ?
                        $config['tempFiles'] :
                        $tempPath = self::TMP_PATH;

        if (!file_exists($tempPath))
            mkdir($tempPath);

        $length = strlen($content);
        $status = null;
        $fileName = null;

        if ($length > 0)
        {
            $fileName = $id . '-' . time() . '.json';
            $filePath = $tempPath . '/' . $fileName;

            $response = file_put_contents($filePath, $content);

            if ($response !== false)
            {
                $status = 'OK';
            }
            else
            {
                $status = 'ERROR';
                $fileName = null;
                $filePath = null;
            }
        }
        else
        {
            $status = 'NO_FILE';
            $fileName = 'no file';
            $filePath = $tempPath;
        }

        return array(
            'status'   => $status,
            'fileName' => $fileName,
            'filePath' => $filePath
        );
    }
    
    public function getTempPath()
    {
        $config = self::getSettings();
        
        $tempPath = !empty($config['tempFiles']) ?
                        $config['tempFiles'] :
                        $tempPath = self::TMP_PATH;

        if (!file_exists($tempPath))
            mkdir($tempPath);
        
        return $tempPath;
    }
    

    /**
     * save messenger config
     * 
     * @param array $settings
     * @return boolean
     */
    public function saveSettings($settings)
    {
        try {
            foreach($settings as $key => $value) {
                Messenger_Config::getInstance()->set($key, $value);
            }
            return true;
        } catch (Tinebase_Exception $ex) {
            Tinebase_Core::getLogger()->err(__METHOD__ . "::" . __LINE__ . ":: Cannot save settings: ".$ex->getMessage());
            return false;
        }
    }

    /**
     * Gets App settings
     * @return array
     */
    public function getSettings()
    {
	$config = Messenger_Config::getInstance();
        return array(
            Messenger_Config::DOMAIN                => $config->get(Messenger_Config::DOMAIN),
            Messenger_Config::RESOURCE              => $config->get(Messenger_Config::RESOURCE),
            Messenger_Config::FORMAT                => $config->get(Messenger_Config::FORMAT),
            Messenger_Config::SERVER_URL            => $config->get(Messenger_Config::SERVER_URL),
            Messenger_Config::TEMP_FILES            => $config->get(Messenger_Config::TEMP_FILES),
            Messenger_Config::DISABLE_VIDEO_CHAT    => $config->get(Messenger_Config::DISABLE_VIDEO_CHAT)
        );
    }
    
    private static function chatHistoryStyle()
    {
        return '<style>
                 .chat-message-notify {
                    padding: 2px 4px;
                 }
                 .chat-message-notify .chat-user-msg {
                    padding: 0 5px;
                    font-size: 100%;
                    font-weight: bold;
                    color: #333;
                 }
                 .chat-message-balloon .chat-user {
                     width: 50px;
                     padding: 8px 3px 3px 3px;
                     text-align: center;
                     font-size: 100%;
                     font-weight: bold;
                     float: left;
                     word-break: break-all;
                     overflow: hidden;
                 }
                 .chat-message-balloon .chat-user-msg {
                     font-size: 110%;
                     float: left;
                     padding: 7px 15px;
                     word-break: break-all;
                     -moz-border-radius: 10px;
                     -webkit-border-radius: 10px;
                 }
                 .chat-user-timestamp {
                    font-size: 10px;
                    color: #777;
                 }
                 .chat-user-balloon {
                    padding-left: 14px;
                    float: left;
                    max-width: 70%;
                    background-repeat: no-repeat;
                    background-position-y: 6px;
                    margin-top: 6px;
                    background-image: url("Messenger/res/balloon-pointer.png");
                 }
                 .color-1 .chat-user-msg { background-color: #8fb1e8 }
                 .color-1 .chat-user, .color-1 .x-tree-node-anchor span { color: #8fb1e8!important}
                 .color-1 .chat-user-balloon { background-position-x: 0 }

                 .color-2 .chat-user-msg { background-color: #98d96c }
                 .color-2 .chat-user, .color-2 .x-tree-node-anchor span  { color: #98d96c!important}
                 .color-2 .chat-user-balloon { background-position-x: -15px }

                 .color-3 .chat-user-msg { background-color: #fff }
                 .color-3 .chat-user, .color-3 .x-tree-node-anchor span  { color: #fff!important}
                 .color-3 .chat-user-balloon { background-position-x: -30px }

                 .color-4 .chat-user-msg { background-color: #ffb380 }
                 .color-4 .chat-user, .color-4 .x-tree-node-anchor span  { color: #ffb380!important}
                 .color-4 .chat-user-balloon { background-position-x: -45px }

                 .color-5 .chat-user-msg { background-color: #ffe680 }
                 .color-5 .chat-user, .color-5 .x-tree-node-anchor span  { color: #ffe680!important}
                 .color-5 .chat-user-balloon { background-position-x: -60px }

                 .color-6 .chat-user-msg { background-color: #ff8080 }
                 .color-6 .chat-user, .color-6 .x-tree-node-anchor span  { color: #ff8080!important}
                 .color-6 .chat-user-balloon { background-position-x: -75px }

                 .color-7 .chat-user-msg { background-color: #ac9393 }
                 .color-7 .chat-user, .color-7 .x-tree-node-anchor span  { color: #ac9393!important}
                 .color-7 .chat-user-balloon { background-position-x: -90px }
                </style>';
    }
    
}
