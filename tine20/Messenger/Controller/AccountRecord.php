<?php
/**
 * AccountRecord controller for Messenger application
 * 
 * @package     Messenger
 * @subpackage  Controller
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 *
 */

/**
 * AccountRecord controller class for Messenger application
 * 
 * @package     Messenger
 * @subpackage  Controller
 */
class Messenger_Controller_AccountRecord extends Tinebase_Controller_Record_Abstract
{
    /**
     * the constructor
     *
     * don't use the constructor. use the singleton 
     */
    private function __construct() {
        $this->_applicationName = 'Messenger';
        $this->_backend = new Messenger_Backend_AccountRecord();
        $this->_modelName = 'Messenger_Model_AccountRecord';
        $this->_doContainerACLChecks = false;
    }
    
    /**
     * holds the instance of the singleton
     *
     * @var Messenger_Controller_AccountRecord
     */
    private static $_instance = NULL;
    
    /**
     * the singleton pattern
     *
     * @return Messenger_Controller_AccountRecord
     */
    public static function getInstance() 
    {
        if (self::$_instance === NULL) {
            self::$_instance = new Messenger_Controller_AccountRecord();
        }
        
        return self::$_instance;
    }        

}
