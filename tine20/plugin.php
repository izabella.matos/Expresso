<?php
/**
 * Tine 2.0 plugin change script
 * - This script change plugin settings
 *
 * @package     HelperScripts
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Flávio Gomes da Silva Lisboa <flavio.lisboa@serpro.gov.br>
 * @copyright   Copyright (c) 2015 Metaways Infosystems GmbH (http://www.metaways.de)
 * @copyright   Copyright (c) 2015 Serpro (http://www.serpro.gov.br)
 * @version     $Id$
 * @todo allow changes for all available plugins
 *
 */

if (php_sapi_name()!=='cli') {
    echo 'Available only to administrators';
    exit;
}

main();

/**
 * Recognize desired operation
 */
function main()
{

    prepareEnvironment();

    try {
        $opts = new Zend_Console_Getopt(array(
                'coif|a'=>'config only into filesystem',
                'cbifad|b'=>'config both into filesystem and database',
                'me|c'=>'multidomain enabled',
                'md|d'=>'multidomain disabled',
                'dog|e'=>'domain data override global data',
                'god|f'=>'global data override domain data',
                'help'=>'help option with no required parameter'
            )
        );
        $opts->parse();
    } catch (Zend_Console_Getopt_Exception $e) {
        echo $e->getUsageMessage();
        exit;
    }

    if($opts->getOption('help')) {
        die("ERROR: ".$opts->getUsageMessage()."\n");
    }

    $coif = $opts->getOption('a');
    if(!empty($coif)){
        setConfigOnlyIntoFilesystem(TRUE);
    }
    $cbifad = $opts->getOption('b');
    if(!empty($cbifad)){
        setConfigOnlyIntoFilesystem(FALSE);
    }
    $me = $opts->getOption('c');
    if(!empty($me)){
        setMultidomain(TRUE);
    }
    $md = $opts->getOption('d');
    if(!empty($md)){
        setMultidomain(FALSE);
    }

    $dog = $opts->getOption('e');
    if(!empty($dog)){
        domainOverrideGlobal(TRUE);
    }

    $god = $opts->getOption('f');
    if(!empty($god)){
        domainOverrideGlobal(FALSE);
    }
}

/**
 * Sets the include path and loads autoloader classes
 */
function prepareEnvironment()
{
    $paths = array(
            realpath(__DIR__),
            realpath(__DIR__ . '/library'),
            get_include_path()
    );
    set_include_path(implode(PATH_SEPARATOR, $paths));

    require_once 'Zend/Loader/Autoloader.php';
    $autoloader = Zend_Loader_Autoloader::getInstance();
    $autoloader->setFallbackAutoloader(true);
    Tinebase_Autoloader::initialize($autoloader);
}

/**
 *
 * @param boolean $enabled
 */
function setConfigOnlyIntoFilesystem($enabled)
{
    changeSetting('Tinebase_Config::setConfigOnlyIntoFilesystem', 'Changing configuration storage', $enabled);
}

/**
 *
 * @param boolean $enabled
 */
function setMultidomain($enabled)
{
    changeSetting('Tinebase_Config_Manager::setMultidomain', 'Changing multidomain status', $enabled);
}

/**
 * change precedence of configuration
 *
 * @param boolean $enabled
 */
function domainOverrideGlobal($enabled)
{
    changeSetting('Tinebase_Config::domainOverrideGlobal','Changing configuration precedence', $enabled);
}

/**
 * Change the argument of a call method
 *
 * @param string $setting
 * @param string $message
 * @param boolean $enabled
 */
function changeSetting($setting, $message, $enabled)
{
    echo "\n$message...";
    try {
        $plugins = file_get_contents(__DIR__ . '/init_plugins.php');
        $new = $enabled ? 'TRUE' : 'FALSE';
        $current = $enabled ? 'FALSE' : 'TRUE';
        $searched = '/'.$setting.'\((TRUE|FALSE)\);/';
        $replaced = "$setting($new);";
        if (preg_match($searched, $plugins) == 0){
            $plugins .= "\n$replaced";
        } else {
		    $plugins = preg_replace($searched, $replaced, $plugins);
		}
        $plugins = file_put_contents(__DIR__ . '/init_plugins.php', $plugins);
        echo "\nConfig item now is: \n$replaced\n";
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}
